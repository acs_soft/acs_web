﻿using System.Web;

namespace ACS
{
    public class CookieManagement
    {

        public Cookie checkcookies()
        {
            Cookie c = new Cookie();

            if (HttpContext.Current.Request.Cookies["QUNTX0NPT0tJRVNfTkFNRQ"] != null)
            {
                c.code = HttpContext.Current.Request.Cookies["QUNTX0NPT0tJRVNfTkFNRQ"]["solitary_code"].ToString();
                c.role = HttpContext.Current.Request.Cookies["QUNTX0NPT0tJRVNfTkFNRQ"]["solitary_role"].ToString();
                c.token = HttpContext.Current.Request.Cookies["QUNTX0NPT0tJRVNfTkFNRQ"]["solitary_token"].ToString();
                c.name = HttpContext.Current.Request.Cookies["QUNTX0NPT0tJRVNfTkFNRQ"]["name"].ToString().Trim();
                c.mobile = HttpContext.Current.Request.Cookies["QUNTX0NPT0tJRVNfTkFNRQ"]["mobile"].ToString();
                c.emailid = HttpContext.Current.Request.Cookies["QUNTX0NPT0tJRVNfTkFNRQ"]["emailid"].ToString();
                c.designation = HttpContext.Current.Request.Cookies["QUNTX0NPT0tJRVNfTkFNRQ"]["designation"].ToString();
                c.url = HttpContext.Current.Request.Cookies["QUNTX0NPT0tJRVNfTkFNRQ"]["url"].ToString();
                c.dashboardpage = HttpContext.Current.Request.Cookies["QUNTX0NPT0tJRVNfTkFNRQ"]["dashboardpage"].ToString();
                c.last_name = HttpContext.Current.Request.Cookies["QUNTX0NPT0tJRVNfTkFNRQ"]["last_name"].ToString();



                return c;

            }
            else
            {
                HttpContext.Current.Response.Redirect("~/login.aspx");
                return null;
            }



        }
    }
    public class Cookie
    {
        public string code { get; set; }
        public string role { get; set; }
        public string token { get; set; }
        public string name { get; set; }
        public string mobile { get; set; }
        public string emailid { get; set; }
        public string designation { get; set; }
        public string url { get; set; }
        public string dashboardpage { get; set; }
        public string last_name { get; set; }
        public string bearer_token { get; set; }
    }
}