$(function() {
	'use strict';
	
  $('.form-control').on('input', function() {
	  var $field = $(this).closest('.form-group');
	  if (this.value) {
	    $field.addClass('field--not-empty');
	  } else {
	    $field.removeClass('field--not-empty');
	  }
	});

});



function sucess_msg(customerrormsg) {
    $('#commonerrorwrap').css('background-color', '#4CAF50');
    $("#commonerrorwrap").fadeIn('slow');
    $('#commonerrorwrap').html("<i class='fa fa-check-square'></i>&nbsp;&nbsp;<label id='lblerrorpage'>" + customerrormsg + "</label>");
    $('#commonerrorwrap').modal('hide');
    setTimeout(function () {
        $("#commonerrorwrap").fadeOut('slow');
    }, 2000);
}

function info_msg(customerrormsg) {
    $('#commonerrorwrap').css('background-color', '#00BCD4');
    $("#commonerrorwrap").fadeIn('slow');
    $('#commonerrorwrap').html("<i class='fa fa-info-circle'></i>&nbsp;&nbsp;<label id='lblerrorpage'>" + customerrormsg + "</label>");
    $('#commonerrorwrap').modal('hide');
    setTimeout(function () {
        $("#commonerrorwrap").fadeOut('slow');
    }, 2000);
}

function error_msg(customerrormsg) {
    $('#commonerrorwrap').css('background-color', '#F44336');
    $("#commonerrorwrap").fadeIn('slow');
    $('#commonerrorwrap').html("<i class='fa fa-window-close'></i>&nbsp;&nbsp;<label id='lblerrorpage'>" + customerrormsg + "</label>");
    $('#commonerrorwrap').modal('hide');
    setTimeout(function () {
        $("#commonerrorwrap").fadeOut('slow');
    }, 2000);
}