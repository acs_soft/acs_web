﻿using System;
using System.Web;

namespace ACS.admin
{
    public partial class logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            foreach (string key in Request.Cookies.AllKeys)
            {
                HttpCookie c = Request.Cookies[key];
                c.Expires = DateTime.Now.AddMonths(-1);
                Response.AppendCookie(c);
            }

            Response.Redirect("~/login.aspx");
            //if (Request.Cookies["EquinoxCRM"] != null)
            //{
            //    Response.Cookies["EquinoxCRM"].Expires = DateTime.Now.AddDays(-1);
            //    Response.Redirect("~/login.aspx");
            //}
            //else
            //{
            //    Response.Redirect("~/login.aspx");
            //}

        }
    }
}