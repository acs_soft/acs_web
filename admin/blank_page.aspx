﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/ACS.Master" AutoEventWireup="true" CodeBehind="blank_page.aspx.cs" Inherits="ACS.admin.blank_page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper">
        <div class="row p-2">
            <div class="col-md-3">
            </div>
            <div class="col-md-4 p-2">

                <input type="text" class="form-control input-lg p-2" name="fname" id="sea" placeholder="Enter Keyword" style="width: 100%;">
            </div>
            <div class="col-md-2 p-2">


                <input type="button" class="btn btn-primary" value="UNIVERSAL SEARCH" onclick="showDetails()" style="padding: 10px 10px;">
            </div>
            <div class="col-md-12">
                <div class="loader mt-5 mb-5" style="margin: 0px auto; display: none;"></div>
            </div>
        </div>
        <div class="row  p-2  mb-2 card-body" id="s1" style="display: none; background-color: white; border-radius: 5px;">

            <div class="col-md12 mb-2">
                <h4>Google Search Results</h4>
            </div>
            <div class="col-md-4 mb-2"><i class="fa fa-google fa-2x"></i><a class="btn mb-2" id="btn1" target="_blank" href="#"></a></div>
            <div class="col-md-4 mb-2"><i class="fa fa-google fa-2x"></i><a class="btn mb-2" id="btn2" target="_blank" href="#"></a></div>
            <div class="col-md-4 mb-2"><i class="fa fa-google fa-2x"></i><a class="btn mb-2" id="btn3" target="_blank" href="#"></a></div>
            <div class="col-md-4 mb-2"><i class="fa fa-google fa-2x"></i><a class="btn mb-2" id="btn4" target="_blank" href="#"></a></div>
            <div class="col-md-4 mb-2"><i class="fa fa-google fa-2x"></i><a class="btn mb-2" id="btn5" target="_blank" href="#"></a></div>
            <div class="col-md-4 mb-2"><i class="fa fa-google fa-2x"></i><a class="btn mb-2" id="btn6" target="_blank" href="#"></a></div>
        </div>
        <div class="row  p-2  mb-2 card-body" id="s2" style="display: none; background-color: white; border-radius: 5px;">
            <div class="col-md12 mb-2">
                <h4>Social Media Search Results</h4>
            </div>
            <div class="col-md-4 mb-2"><i class="fa fa-facebook-official fa-2x"></i><a class="btn mb-2" id="btnface" target="_blank" href="#"></a></div>
            <div class="col-md-4 mb-2"><i class="fa fa-linkedin fa-2x"></i><a class="btn mb-2" id="btninin" target="_blank" href="#"></a></div>
            <div class="col-md-4 mb-2"><i class="fa fa-twitter fa-2x"></i><a class="btn mb-2" id="btntwitter" target="_blank" href="#"></a></div>
            <div class="col-md-4 mb-2"><i class="fa fa-instagram fa-2x"></i><a class="btn mb-2" id="btninsta" target="_blank" href="#"></a></div>


        </div>

        <div class="row  p-2  mb-2 card-body" style="border-radius: 5px;">

            <div class="col-md-6 p-3" id="s3" style="display: none; background-color: white;">
                <div class="col-md12 mb-2">
                    <h4>My Neta</h4>
                </div>
                <a class="badge badge-dark mb-1" id="wikiopen1" target="_blank" href="#" title="Open In new tab" style="position: absolute; top: 16px; right: 3%; z-index: 999;"><i class="fa fa-external-link fa-1x  p-1"></i><a class=""></a></a>
                <div class="embed-responsive embed-responsive-16by9" style="box-shadow: 0px 0px 10px grey;">
                    <iframe id="mynetaFrame" name="innerFrame" class="embed-responsive-item" frameborder="0" sandbox="allow-scripts allow-popups allow-forms allow-same-origin allow-popups-to-escape-sandbox allow-downloads" frameborder="0" allowfullscreen="" src="https://www.myneta.info/"></iframe>
                </div>


            </div>
            <div class="col-md-6 p-3" id="s4" style="display: none; background-color: white;">
                <div class="col-md12 mb-2">
                    <h4>Wikipedia</h4>
                </div>
                <a class="badge badge-dark mb-1" id="wikiopen" target="_blank" href="#" title="Open In new tab" style="position: absolute; top: 16px; right: 3%; z-index: 999;"><i class="fa fa-external-link fa-1x  p-1"></i><a class=""></a></a>
                <div class="embed-responsive embed-responsive-16by9" style="box-shadow: 0px 0px 10px grey;">
                    <iframe id="wikiFrame" name="innerFrame" class="embed-responsive-item" sandbox="allow-scripts allow-popups allow-forms allow-same-origin allow-popups-to-escape-sandbox allow-downloads" frameborder="0" allowfullscreen="" src="https://en.wikipedia.org/wiki" style="overflow: auto;"></iframe>
                </div>
            </div>
            <div class="col-md-6 p-3" id="s5" style="display: none; background-color: white;">
                <div class="col-md12 mb-2">
                    <h4>Local Database</h4>
                </div>
                <a class="badge badge-dark mb-1" id="wikiopen" target="_blank" href="#" title="Open In new tab" style="position: absolute; top: 16px; right: 3%; z-index: 999;"><i class="fa fa-external-link fa-1x  p-1"></i><a class=""></a></a>
                <div class="embed-responsive embed-responsive-16by9" style="box-shadow: 0px 0px 10px grey;">
                    <iframe id="wikiFrame" name="innerFrame" class="embed-responsive-item" sandbox="allow-scripts allow-popups allow-forms allow-same-origin allow-popups-to-escape-sandbox allow-downloads" frameborder="0" allowfullscreen="" src="https://en.wikipedia.org/wiki" style="overflow: auto;"></iframe>
                </div>
            </div>
        </div>

        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
            <div class="d-sm-flex justify-content-center justify-content-sm-between">
                <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">WWTECH</span>
                <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Copyright © 2021. All rights reserved.</span>
            </div>
        </footer>
        <!-- partial -->
    </div>

     <script type="text/javascript">
         function showgoogleDetails() {
             $('#s1').show();
             $('#s2').hide();
             $('#s3').hide();
             $('#s4').hide();
             $('#s5').hide();
         }
         function showsocialDetails() {
             $('#s1').hide();
             $('#s2').show();
             $('#s3').hide();
             $('#s4').hide();
             $('#s5').hide();

         }
         function showmynetaDetails() {
             $('#s1').hide();
             $('#s2').hide();
             $('#s3').show();
             $('#s4').hide();
             $('#s5').hide();

         }
         function showwikiDetails() {
             $('#s1').hide();
             $('#s2').hide();
             $('#s3').hide();
             $('#s4').show();
             $('#s5').hide();

         }
         function showexistingDetails() {
             $('#s1').hide();
             $('#s2').hide();
             $('#s3').hide();
             $('#s4').hide();
             $('#s5').show();

         }
         function showDetails() {
             $('#s1').hide();
             $('#s2').hide();
             $('#s3').hide();
             $('#s4').hide();
             $('#s5').hide();

             var a = document.getElementById('sea').value;
             if (a == '' || a == ' ') {
                 info_msg("Please enter keyword !!");
             }
             else {
                 $('.loader').show();
                 setTimeout(function () {
                     $('.loader').hide();
                     $('#s1').show();
                     $('#s2').show();
                     $('#s3').show();
                     $('#s4').show();
                     $('#s5').show();


                     var a1 = "Any fraud " + a;
                     var a2 = "Any bank notice against " + a;
                     var a3 = "Any court case against " + a;
                     var a4 = "Any criminal case " + a;
                     var a5 = "arrtsed " + a;
                     var a6 = "bank auction notice " + a;
                     var awiki = a;

                     var wikiurl = 'https://en.wikipedia.org/wiki/' + awiki;
                     var mynetaFrame = 'https://www.myneta.info/search_myneta.php?q=' + awiki;
                     var btn1 = 'https://www.google.com/search?q=' + a1;
                     var btn2 = 'https://www.google.com/search?q=' + a2;
                     var btn3 = 'https://www.google.com/search?q=' + a3;
                     var btn4 = 'https://www.google.com/search?q=' + a4;
                     var btn5 = 'https://www.google.com/search?q=' + a5;
                     var btn6 = 'https://www.google.com/search?q=' + a6;

                     // var btnface = 'https://www.facebook.com/search/top?q=' + a;
                     //var btninin = 'https://www.linkedin.com/search/results/people/?keywords=' + a;

                     var btnface = 'https://www.facebook.com/public/' + a;
                     var btninin = 'https://www.linkedin.com/search/results/all/?keywords=' + a;
                     var btntwitter = 'https://www.google.com/search?q=Twitter ' + a;
                     var btninsta = 'https://www.google.com/search?q=Instagram ' + a;




                     $('#wikiFrame').attr('src', wikiurl);
                     $('#wikiopen').attr('href', wikiurl);
                     $('#wikiopen1').attr('href', mynetaFrame);
                     $('#mynetaFrame').attr('src', mynetaFrame);

                     $('#btn1').attr('href', btn1);
                     $('#btn2').attr('href', btn2);
                     $('#btn3').attr('href', btn3);
                     $('#btn4').attr('href', btn4);
                     $('#btn5').attr('href', btn5);
                     $('#btn6').attr('href', btn6);
                     $('#btnface').attr('href', btnface);
                     $('#btninin').attr('href', btninin);
                     $('#btntwitter').attr('href', btntwitter);
                     $('#btninsta').attr('href', btninsta);

                     $("#btn1").text(a1);
                     $("#btn2").text(a2);
                     $("#btn3").text(a3);
                     $("#btn4").text(a4);
                     $("#btn5").text(a5);
                     $("#btn6").text(a6);

                     $("#btnface").text("Facebook " + a);
                     $("#btninin").text("Linkedin " + a);
                     $("#btntwitter").text("Twitter " + a);
                     $("#btninsta").text("Instagram " + a);
                 }, 2000);
             }

         }</script>
</asp:Content>


