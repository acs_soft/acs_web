﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ACS.admin
{
    public partial class Bulk_Upload_Disqualified_director : System.Web.UI.Page
    {
        string fileLocation = "";
        List<string> Errorlist = new List<string>();
        List<string> successList = new List<string>();
        List<string> FailedList = new List<string>(0);
        DataTable dt12 = new DataTable();

        bool Errorfree;
        string innererrorstring;
        string innersuccessstring;
        string innerfailurestring;
        Cookie cookievar = new Cookie();

        //protected void Button2_Click(object sender, EventArgs e)
        //{
        //    Response.Redirect("VerifierDashboard.aspx", false);
        //}

        protected void Page_Load(object sender, EventArgs e)
        {


            CookieManagement ck = new CookieManagement();
            cookievar = ck.checkcookies();

            dt12.Columns.AddRange(new DataColumn[9]
            {


                new DataColumn("DIN", typeof(string)),
                new DataColumn("DIRECTOR_NAME", typeof(string)),
                new DataColumn("COMPANY_NAME", typeof(string)),
                new DataColumn("CIN", typeof(string)),
                new DataColumn("ROC_LOCATION", typeof(string)),
                new DataColumn("COMPANY_STATUS", typeof(string)),
                new DataColumn("STATUS", typeof(string)),
                   new DataColumn("PERIOD_FROM", typeof(string)),
                      new DataColumn("PERIOD_TILL", typeof(string)),



        });

            duplicatedsitewrapper.Visible = false;
        }


        protected void Check_Click(object sender, EventArgs e)
        {
            bool isTransferToInfusion;
            bool.TryParse(HnSendToInfusion.Value, out isTransferToInfusion);

            List<string> LeadMobileNumber = new List<string>();



            successlistdiv.InnerHtml = "";
            successcount.InnerHtml = "";
            failurecount.InnerHtml = "";
            //failurelistdiv.InnerHtml = "";
            errorcount.InnerHtml = "";
            errorListdiv.InnerHtml = "";
            try
            {
                //if File is not selected then return  
                if (FileUpload1.HasFile)
                {
                    string fileExtension = System.IO.Path.GetExtension(FileUpload1.FileName);


                    //If file is not in excel format then return  
                    if (fileExtension != ".xls" && fileExtension != ".xlsx")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>document.getElementById('error').style.display='block';document.getElementById('error').style.backgroundColor='#00BCD4';setTimeout(function() {$('#error').fadeOut('slow');}, 5000); </script>", false);
                        errorwrap.Text = "Select xls or xlsx file onlyss";


                    }
                    else
                    {

                        //Get the File name and create new path to save it on server  
                        fileLocation = Server.MapPath("sitexls\\") + FileUpload1.FileName;
                        FileUpload1.SaveAs(fileLocation);

                        //Create the QueryString for differnt version of fexcel file  
                        string strConn = "";
                        switch (fileExtension)
                        {
                            case ".xls": //Excel 1997-2003  
                                strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\"";
                                break;
                            //case ".csv": //Excel 1997-2003  
                            //    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation+ ";Extended Properties=\"Text;HDR=Yes;FORMAT=Delimited\"";
                            //    break;
                            case ".xlsx": //Excel 2007-2010  
                                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 12.0 xml;HDR=Yes;IMEX=1\"";
                                break;
                        }

                        //Get the data from the excel sheet1 which is default  
                        string query = "select * from  [Sheet1$] where DIN <> '' or DIN <> null";
                        OleDbConnection objConn;
                        OleDbDataAdapter oleDA;
                        //DataTable dt = new DataTable();

                        objConn = new OleDbConnection(strConn);
                        objConn.Open();
                        oleDA = new OleDbDataAdapter(query, objConn);
                        using (DataSet Uploadds = new DataSet())
                        {
                            oleDA.Fill(Uploadds);
                            objConn.Close();
                            oleDA.Dispose();
                            objConn.Dispose();
                            int excelrow = Uploadds.Tables[0].Rows.Count;
                            if (excelrow > 0)
                            {
                                //Bind the datatable to the Grid  
                                GridView1.DataSource = Uploadds;
                                GridView1.DataBind();

                                bool Headersverified = VadlidateHeaders();

                                if (!Errorlist.Any())


                                {
                                    for (int i = 0; i < Uploadds.Tables[0].Rows.Count; i++)
                                    {


                                        string DIN = Uploadds.Tables[0].Rows[i]["DIN"].ToString();
                                        string DIRECTOR_NAME = Uploadds.Tables[0].Rows[i]["DIRECTOR_NAME"].ToString();
                                        string COMPANY_NAME = Uploadds.Tables[0].Rows[i]["COMPANY_NAME"].ToString();
                                        string CIN = Uploadds.Tables[0].Rows[i]["CIN"].ToString();
                                        string ROC_LOCATION = Uploadds.Tables[0].Rows[i]["ROC_LOCATION"].ToString();
                                        string COMPANY_STATUS = Uploadds.Tables[0].Rows[i]["COMPANY_STATUS"].ToString();
                                        string STATUS = Uploadds.Tables[0].Rows[i]["STATUS"].ToString();
                                        string PERIOD_FROM = Uploadds.Tables[0].Rows[i]["PERIOD_FROM"].ToString();
                                        string PERIOD_TILL = Uploadds.Tables[0].Rows[i]["PERIOD_TILL"].ToString();

                                        try
                                        {
                                            errorwrap.Text = "";
                                            CookieManagement ck = new CookieManagement();
                                            cookievar = ck.checkcookies();
                                            string code = cookievar.code;

                                            DataSet ds = new DataSet();
                                            DataSet ds1 = new DataSet();
                                            string str = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

                                            using (SqlConnection con = new SqlConnection(str))
                                            {
                                                using (SqlCommand cmd = new SqlCommand("USP_DISQUALIFIED_DIRECTOR_MANAGMENT"))
                                                {
                                                    cmd.CommandType = CommandType.StoredProcedure;
                                                    cmd.Connection = con;
                                                    cmd.Parameters.AddWithValue("@type", "INSERT_DISQUALIFIED_DIRECTOR");

                                                    cmd.Parameters.AddWithValue("@DIN", DIN);
                                                    cmd.Parameters.AddWithValue("@DIRECTOR_NAME", DIRECTOR_NAME);
                                                    cmd.Parameters.AddWithValue("@COMPANY_NAME", COMPANY_NAME);
                                                    cmd.Parameters.AddWithValue("@CIN", CIN);
                                                    cmd.Parameters.AddWithValue("@ROC_LOCATION", ROC_LOCATION);
                                                    cmd.Parameters.AddWithValue("@COMPANY_STATUS", COMPANY_STATUS);
                                                    cmd.Parameters.AddWithValue("@STATUS", STATUS);
                                                    cmd.Parameters.AddWithValue("@PERIOD_FROM", PERIOD_FROM);
                                                    cmd.Parameters.AddWithValue("@PERIOD_TILL", PERIOD_TILL);
                                                    cmd.Parameters.AddWithValue("@USER_CODE", "11111111-1111-1111-1111-111111111111");

                                                    con.Open();
                                                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                                                    {
                                                        sda.Fill(ds);
                                                    }

                                                    if (ds.Tables[0].Rows[0][0].ToString() == "0")
                                                    {
                                                        if (ds.Tables[1].Rows.Count > 0)
                                                        {
                                                            FailedList.Add("DIN :" + DIN + " is already Exist.");

                                                            // crtotalno.InnerText = RowCount1.ToString();

                                                            dt12.Rows.Add(

                                                                ds.Tables[1].Rows[0]["Candidate"],
                                                                ds.Tables[1].Rows[0]["Constituency"],
                                                                ds.Tables[1].Rows[0]["Party"],
                                                                ds.Tables[1].Rows[0]["Criminal_Case"],
                                                                ds.Tables[1].Rows[0]["Education"],
                                                                ds.Tables[1].Rows[0]["Total_Assets"],
                                                                ds.Tables[1].Rows[0]["Liabilities"]
                                                             

                                                                );

                                                            GetnewData(dt12);
                                                        }
                                                    }
                                                    else
                                                    {
                                                      


                                                        successList.Add("DIN No :" + DIN + " is uploaded Successfully");


                                                        LeadMobileNumber.Add(DIN);

                                                       

                                                    }


                                                }
                                            }


                                        }

                                        catch (Exception ex)
                                        {
                                            string StrErrorMsg = ex.Message;
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>document.getElementById('error').style.display='block';setTimeout(function() {$('#error').fadeOut('slow');}, 10000); </script>", false);
                                            errorwrap.Text = StrErrorMsg;

                                        }


                                    }
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>document.getElementById('error').style.display='block';setTimeout(function() {$('#error').fadeOut('slow');}, 10000); </script>", false);
                                    errorwrap.Text = "Kindly Find Upload Status Below";
                                    string Msg = string.Empty;

                                    Successdisplaydiv.Style.Add("display", "block");
                                    for (int j = 0; j < successList.Count; j++)
                                    {
                                        innersuccessstring += "<div class='successlable p-1' >" + successList[j] + "</div>";
                                    }
                                    for (int j = 0; j < FailedList.Count; j++)
                                    {
                                        innerfailurestring += "<div class='errorlable p-1'>" + FailedList[j] + "</div>";
                                    }
                                    successlistdiv.InnerHtml = innersuccessstring;
                                    successcount.InnerHtml = "<span class='p-3'>Uploaded Successfully : " + successList.Count + "</span>";
                                    if (!string.IsNullOrEmpty(Msg))
                                        InfusionMsg.InnerHtml = "<span class='p-3'> Infusion Alert : " + Msg + "</span>";
                                    else
                                        InfusionMsg.InnerHtml = "";
                                    failurecount.InnerHtml = "<span class='p-3'>Not Uploaded : " + FailedList.Count + "</span>";

                                    errordisplaydiv.InnerHtml = "";
                                    //Delete the excel file from the server  
                                    File.Delete(fileLocation);
                                }

                                else
                                {

                                    errordisplaydiv.Style.Add("display", "block");


                                    for (int j = 0; j < Errorlist.Count; j++)
                                    {
                                        innererrorstring += "<div class='errorlable'>* " + Errorlist[j] + "</div>";
                                    }

                                    errorcount.InnerHtml = "<span>" + Errorlist.Count + " </span>";
                                    errorListdiv.InnerHtml = innererrorstring;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>document.getElementById('error').style.display='block';document.getElementById('error').style.backgroundColor='#F44336';setTimeout(function() {$('#error').fadeOut('slow');}, 5000); </script>", false);
                                errorwrap.Text = "Empty Excel Sheet !!";
                            }
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>document.getElementById('error').style.display='block';document.getElementById('error').style.backgroundColor='#00BCD4';setTimeout(function() {$('#error').fadeOut('slow');}, 5000); </script>", false);
                    errorwrap.Text = "Please select File !!";

                }
            }
            catch (Exception ex)
            {
                //lbl_error_test.Text = ex.ToString();
                errorwrap.Text = ex.ToString() + "Select valid xls file format";
            }

        }



        public void GetnewData(DataTable dt21)
        {


            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {

                DataTable copyDataTable;
                copyDataTable = dt21.Copy();
                GridView3.DataSource = dt21;
                GridView3.DataBind();
                duplicatedsitewrapper.Visible = true;



            }
            catch (Exception ex)
            {
                Console.Write(ex);

            }



        }

  
        protected void btn_export(object sender, EventArgs e)
        {
            ExportGridToExcel();
        }
        private void ExportGridToExcel()
        {
            Response.Clear();
            Response.Buffer = true;
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";
            string FileName = "DIRECTOR_NAME" + DateTime.Now + ".xls";
            StringWriter strwritter = new StringWriter();
            HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
            GridView3.GridLines = GridLines.Both;
            GridView3.HeaderStyle.Font.Bold = true;
            GridView3.RenderControl(htmltextwrtter);
            Response.Write(strwritter.ToString());
            Response.End();

        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                bool rowerror = false;
                int rownumber = e.Row.RowIndex;
                rownumber = rownumber + 1;

                DataRow row = ((DataRowView)e.Row.DataItem).Row;

                string LEADNAME = row.Field<string>("DIN");
                if (LEADNAME == null) { rowerror = true; Errorlist.Add("DIN is null at Row " + rownumber + ""); e.Row.Cells[0].BackColor = System.Drawing.Color.Salmon; }



               // string EMAILID = row.Field<string>("EMAIL");
               // if (EMAILID == null) { rowerror = true; Errorlist.Add("EMAIL is null at Row " + rownumber + ""); e.Row.Cells[2].BackColor = System.Drawing.Color.Salmon; }

                //   string MOBILEno = row.Field<string>("MOBILE");
                // if (MOBILEno == null) { rowerror = true; Errorlist.Add("MOBILE is null at Row " + rownumber + ""); e.Row.Cells[1].BackColor = System.Drawing.Color.Salmon; }


                //string LEADSOURCEID = row.Field<string>("LEAD_SOURCE");
                //if (LEADSOURCEID == null) { rowerror = true; Errorlist.Add("LEAD SOURCE is null at Row " + rownumber + ""); e.Row.Cells[3].BackColor = System.Drawing.Color.Salmon; }


                //try
                //{
                //    double? sitemobilenumber = row.Field<double?>("MOBILE");
                //    //  string sitemobilenumber = row.Field<string>("MOBILE").ToString();

                //    string SITEMOBILE = sitemobilenumber.ToString();
                //    if (SITEMOBILE == "") { rowerror = true; Errorlist.Add("Site Mobile is null at Row " + rownumber + ""); e.Row.Cells[1].BackColor = System.Drawing.Color.Salmon; }
                //  //  else if (Ismobile(SITEMOBILE)) { rowerror = true; Errorlist.Add("Mobile is in Invalid format at Row " + rownumber + ""); e.Row.Cells[1].BackColor = System.Drawing.Color.Salmon; }

                //}
                //catch (InvalidCastException)
                //{
                //    { rowerror = true; Errorlist.Add("Mobile number not in valid format " + rownumber + ""); e.Row.Cells[1].BackColor = System.Drawing.Color.Salmon; }
                //}



                e.Row.BackColor = rowerror ? System.Drawing.Color.Yellow : System.Drawing.Color.White;
                Errorfree = rowerror ? false : true;
            }
        }

        public bool VadlidateHeaders()
        {

            if (GridView1.HeaderRow.Cells[0].Text != "DIN") { return false; }
            else if (GridView1.HeaderRow.Cells[1].Text != "DIRECTOR_NAME") { return false; }
            else if (GridView1.HeaderRow.Cells[2].Text != "COMPANY_NAME") { return false; }
            else if (GridView1.HeaderRow.Cells[3].Text != "CIN") { return false; }
            else if (GridView1.HeaderRow.Cells[4].Text != "ROC_LOCATION") { return false; }
            else if (GridView1.HeaderRow.Cells[5].Text != "COMPANY_STATUS") { return false; }
            else if (GridView1.HeaderRow.Cells[6].Text != "STATUS") { return false; }
            else if (GridView1.HeaderRow.Cells[7].Text != "PERIOD_FROM") { return false; }
            else if (GridView1.HeaderRow.Cells[8].Text != "PERIOD_TILL") { return false; }
            //else if (GridView1.HeaderRow.Cells[11].Text != "PHONENO") { return false; }
            //else if (GridView1.HeaderRow.Cells[12].Text != "ALTERNATE_PH_NO") { return false; }
            //else if (GridView1.HeaderRow.Cells[13].Text != "ALTERNATE_MOBILE") { return false; }
            //else if (GridView1.HeaderRow.Cells[14].Text != "POC ALTERNATE_EMAIL") { return false; }
            //else if (GridView1.HeaderRow.Cells[15].Text != "LEAD_OWNER") { return false; }
            else { return true; }
        }
        protected bool CheckDate(String date)

        {

            try

            {

                DateTime dt = DateTime.Parse(date);

                return true;
            }

            catch

            {

                return false;

            }

        }
        protected bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return true;
            }
            catch
            {
                return false;
            }
        }
        protected bool Ismobile(string mobile)
        {
            Regex re = new Regex(@"^\+[0-9]{2}\s+[0-9]{2}[0-9]{8}$");
            if (re.IsMatch(mobile))
            {
                return false;
            }
            {
                return true;
            }




        }
        protected bool Ispincode(string pincode)
        {
            Regex re = new Regex("^[0-9]{6}$");
            if (re.IsMatch(pincode)) { return false; }
            return true;
        }

    }
}