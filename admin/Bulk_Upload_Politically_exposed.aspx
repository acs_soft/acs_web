﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/ACS.Master" AutoEventWireup="true" CodeBehind="Bulk_Upload_Politically_exposed.aspx.cs" Inherits="ACS.admin.Bulk_Upload_Politically_exposed" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!-- Css -->
    <link href="css/page.css" rel="stylesheet" />
    <link href="css/checkbox.css" rel="stylesheet" />
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
            background-color: white;
            border: 1px solid gray;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        td {
            background: #fff;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }



        input[type=file] {
            display: block;
            font-size: 1.2em;
            padding: 4px;
            border-radius: 5px;
            width: 100%;
            background-color: white;
        }

      </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="AddUser_errorwrap" class="text-center pt-2 col-md-12 col-sm-6 col-xs-12 mastererror"></div>
  <%--  <div id="loadingdiv" style="position: absolute; z-index: 9999; width: 95%; height: 88%; background-color: white; display: none;">
        <img src="images/loading1.gif" style="position: relative; top: 4rem; left: 25rem; cursor: pointer;" />
    </div>--%>
    <div class="content pt-3 bg_body">

        <form id="form1" runat="server">
            <asp:HiddenField ID="HnSendToInfusion" runat="server" />
           
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
      <img  src="images/upload.jpg" style="position: absolute;right: 2%;width: 250px;"/>
      <h2 class="pb-5">Bulk Upload Politically exposed</h2>
       <div class="row" style="">
                <div class="col-md-3">
                    <asp:FileUpload ID="FileUpload1" runat="server" class="form-control-uniform-custom" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
                </div>
                 <div class="col-md-3">
                    <a href="Bulk_Upload_Politically_exposed.xlsx" class="btn btn-info w-100 text-uppercase" download">Download Example Upload Format</a>
                </div>
                
            </div>
          
            <div class="row" style="">
                
                   <div class="col-md-3 mt-3 mb-4">
                    <asp:Button ID="Check" runat="server" Text="Upload Excel" OnClick="Check_Click" class="btn btn-success w-100 text-uppercase" />

                </div>
                </div>

            <div class="row pt-2">
                <div class="col-md-12">
                    <div class="exceldiv">
                        <div class="card">
                            <div class="card-header header-elements-inline">
                                 
                                <h5 class="card-title"><i class="fa fa-clone fa-1x "></i>&nbsp;&nbsp;Uploaded Sites</h5>
                           
                                <div class="header-elements">
                                    <div class="list-icons">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">

                        <asp:GridView ID="GridView1" AutoGenerateColumns="true" runat="server" OnRowDataBound="GridView1_RowDataBound" CssClass="Grid" Width="600" RowStyle-Wrap="false">
                        </asp:GridView>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-6">
                    <div class="block-head" style="">

                        <span id="successcount" runat="server" style="margin-left: 20px; line-height: 15px; background-color: #4caf50; padding: 0px; color: white; font-size: 1rem; border-radius: 2px;"></span>
                    </div>
                         <div class="block-head" style="">

                        <span id="InfusionMsg" runat="server" style="margin-left: 20px; line-height: 15px; background-color: #4caf50; padding: 0px; color: white; font-size: 1rem; border-radius: 2px;"></span>
                    </div>
                    <div class="block-head" style="">
                        <span id="failurecount" runat="server" style="margin-left: 20px; line-height: 15px; background-color: #f44336; padding: 0px; color: white; font-size: 1rem; border-radius: 2px;"></span>

                    </div>

                </div>

                <div class="col-md-6">
                    <div class="block-head p-1" id="errordisplaydiv" style="background-color: #ffffff; font-size: 1.2rem; color: #f44336; display: none; border-radius: 2px;" runat="server"><span id="errorcount" runat="server"></span> Error</div>

                    <div id="errorListdiv" runat="server" style="background-color: #f44336; color: white; padding: 0px 2rem; border-radius: 2px;">
                    </div>

                </div>
                <div class="col-md-6">
                    <div id="Successdisplaydiv" runat="server" style="display: none; width: 100%; border-radius: 2px;">
                    </div>

                </div>
            </div>

            <div class="row pt-2">
                <div class="col-md-12 col-sm-12" style=" border-radius: 2px;">

                    <div id="duplicatedsitewrapper" runat="server">
                        <div class="card">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title"><i class="fa fa-clone fa-1x "></i>&nbsp;&nbsp;Duplicated Sites</h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="fullscreen"></a>
                                        <%--    <a class="list-icons-item" data-action="remove"></a>--%>
                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <asp:Button Text="Export" ID="btnexport" runat="server" class="btn btn-success mb-2" Style="padding: 3px 12px; background-color: #2461bf; height: 28px;" OnClick="btn_export" />

                                        <asp:GridView ID="GridView3" AutoGenerateColumns="true" runat="server" Width="600" RowStyle-Wrap="false">
                                            <RowStyle CssClass="oddRow" />
                                            <AlternatingRowStyle CssClass="evenRow" />
                                        </asp:GridView>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="" id="successlistdiv" runat="server" style="margin-left: 20px; line-height: 15px; background-color: #4caf50; padding: 0px; color: white; font-size: 1rem; border-radius: 2px;"></div>
                                    </div>
                                    <%--<div class="col-md-6">
                                        <div class="" id="failurelistdiv" runat="server" style="margin-left: 20px; line-height: 15px; background-color: #f44336; padding: 0px; color: white; font-size: 1rem; border-radius: 2px;"></div>
                                    </div>--%>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 text-center pl-2" id="error">
                <asp:Label ID="errorwrap" ForeColor="red" runat="server" class=""> </asp:Label>

            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 text-center pl-2" id="error1">
                        <asp:Label ID="Contacterrorwrap" ForeColor="white" runat="server" class=""> </asp:Label>
                    </div>

  </div>
  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
    <h2>Bulk Upload to sales Coordinator</h2>



  </div>
  <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
 <h2>Bulk Upload to BDE</h2>
  </div>
</div>

      

           
        </form>

    </div>
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="js/cookie.js"></script>

    <script>
       $("#ContentPlaceHolder1_Check").click(function () {
           
            var strconfirm = confirm("Are you sure, you want to Upload this Excel Sheet?");
            if (strconfirm == true) {
                if (document.getElementById("ContentPlaceHolder1_FileUpload1").files.length == 0) {
                    error_msg("Excel File Not Selected.");

                    return false;
                }
                else {

                    $("#loadingdiv").show();
                    return true;
                }
            }
            else {
                return false;
            }
        });


    </script>

</asp:Content>

