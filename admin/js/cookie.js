﻿function getCookie(name) {
    var start = document.cookie.indexOf(name + "=");
    var len = start + name.length + 1;
    try {
        if ((!start) && (name != document.cookie.substring(0, name.length))) {
            return null;
        }
        if (start === -1) return null;
        if (name === 'url') { var end = document.cookie.indexOf(";", len); }
        else { var end = document.cookie.indexOf("&", len); }
        if (end === -1) end = document.cookie.length;
    } catch (err) { }
    return unescape(document.cookie.substring(len, end));
}

function Cookiefunction() {


    var ob =
    {
        code: getCookie("code"),
        role: getCookie("role"),
        token: getCookie("token"),
        name: getCookie("name"),
        mobile: getCookie("mobile"),
        emailid: getCookie("emailid"),
        designation: getCookie("department"),
    
    };
    return ob;
}