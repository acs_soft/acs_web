﻿$("#loginbutton").click(function () {

    setTimeout(function () {
        $("#loadingdiv").show();

        var username = $("#txtusername").val();
        var password = $("#txtpassword").val();

        if (username !== "") {
            if (password !== "") {

                var obj = {};
                obj.username = username;
                obj.password = password;

                $.ajax({
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    url: 'login.aspx/user_login',
                    data: JSON.stringify(obj),
                    async: false,
                    processData: false,
                    success: function (response) {
                        //alert(response.d);
                        if (response.d === "0") {
                            $("#loadingdiv").hide();
                            $("#lblerror1").text("Invalid Username/Mobile No. !!");
                        }
                        else if (response.d === "1") {
                            $("#loadingdiv").hide();
                            $("#lblerror1").text("Invalid Username/Mobile No. !!");
                        }
                        else if (response.d === "2") {
                            $("#loadingdiv").hide();
                            $("#lblerror1").text("Please Enter Password !!");
                        }
                        else if (response.d === "3") {
                            $("#loadingdiv").hide();
                            $("#lblerror1").text("Please Enter Username !!");
                        }

                        else {
                              $("#loadingdiv").show();
                            if (response.d === "") {
                              
                                    document.location.href = "admin/401Error.aspx", true;
                     
                            }
                            else {
                                
                                 
                                    document.location.href = "admin/" + response.d, true;
                               
                            }

                         
                            $("#lblerror1").text("");
                            $("#txtusername").val("");
                            $("#txtpassword").val("");
                        }



                    },
                    error: function () {
                        $("#loadingdiv").hide();
                        alert("Something went wrong. Please try again!");
                    }
                });


            }
            else {
                // $("#lblerror1").text("Please Enter Mobile Number ");
                $("#loadingdiv").hide();
                $("#lblerror1").text("Please Enter Password");
                $("#txtpassword").focus();
            }
        }
        else {
            $("#loadingdiv").hide();
            $("#lblerror1").text("Please Enter Username/Mobile No. ");
            $("#txtusername").focus();
        }
    }, 1000);

});