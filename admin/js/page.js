﻿//=============================================
//Author: <Author, Kalpesh Mapelkar - Full Stack Developer>
//Create date: <Create 05 - 10 - 2019 >
//Description: <Description,this is an auditor register js >
// =============================================

var center_base_url, api_base_url, ck, hubapiurl;
var counterflag = "0";
var samedata = "0";
var appilcationid = "";

$(document).ready(function () {
    center_base_url = "https://crm.equinoxlab.com/Equinox_Center/";
    api_base_url = "https://crm.equinoxlab.com/api/";
    hubapiurl = "http://beta-hub.equinoxlab.com/api/";
    appilcationid = "6301B2B3-EB0C-EA11-A961-000D3A584347";
});


function sendsms(in_owner_name, in_owner_number, in_message, in_to)
{
    var owner_name = in_owner_name;
    var owner_number = in_owner_number;
    var message = in_message;
    var to = in_to;

    var settings = {
        "url": "https://panelv2.cloudshope.com/api/SMS?message=" + message + "&to=" + to + "&senderid=EQXLAB&type=Trans&contentid=1488579",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Authorization": "Bearer WET5M1Q03NITzHNU4WTb64ZADq7aPE0MEtm5W3jJuNEAh1uT1u7N4Ht9qBjVoHbN1h3w5Aer9CCFCFiM"
        },
    };

    $.ajax(settings).done(function (response) {
        console.log(response);

        sucess_msg("Done");
    });
}

//HeatMap
function heatmap(datatabelid, cellnameclass, nvalue) {

    Array.max = function (array) {
        return Math.max.apply(Math, array);
    };

    // get all values
    var counts = $('#' + datatabelid + " tbody ." + cellnameclass).not('.datatabel-head').map(function () {
        return parseInt($(this).text());
    }).get();

    // return max value
    var max = Array.max(counts);

    if (cellnameclass === "cell-val-blue") {
        xr = 241;
        xg = 241;
        xb = 241;

        yr = 40;
        yg = 120;
        yb = 240;
    }
    else if (cellnameclass === "cell-val-orange") {
        xr = 241;
        xg = 241;
        xb = 241;

        yr = 240;
        yg = 94;
        yb = 35;
    }
    else if (cellnameclass === "cell-val-green") {
        xr = 241;
        xg = 241;
        xb = 241;

        yr = 60;
        yg = 186;
        yb = 84;
    }
    else if (cellnameclass === "cell-val-yellow") {
        xr = 241;
        xg = 241;
        xb = 241;

        yr = 244;
        yg = 194;
        yb = 13;
    }
    else if (cellnameclass === "cell-val-red") {
        xr = 241;
        xg = 241;
        xb = 241;

        yr = 255;
        yg = 0;
        yb = 43;
    }
    else {
        xr = 241;
        xg = 241;
        xb = 241;

        yr = 0;
        yg = 124;
        yb = 195;
    }

    n = nvalue;

    // add classes to cells based on nearest 10 value
    $('#' + datatabelid + " ." + cellnameclass).not('.datatabel-head').each(function () {

        var val = parseInt($(this).text());
        var pos = parseInt((Math.round((val / max) * 100)).toFixed(0));
        red = parseInt((xr + ((pos * (yr - xr)) / (n - 1))).toFixed(0));
        green = parseInt((xg + ((pos * (yg - xg)) / (n - 1))).toFixed(0));
        blue = parseInt((xb + ((pos * (yb - xb)) / (n - 1))).toFixed(0));
        clr = 'rgb(' + red + ',' + green + ',' + blue + ')';
        $(this).css({ backgroundColor: clr });
        $(this).addClass("zoom");

    });
}



/*notification*/
function all_notifications(msg) {

    $.notify({

        message: msg,
    },
        {
            type: 'info',
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            },
            placement: {
                from: "top",
                align: "right"
            },

            offset: 20,
            spacing: 10,
            z_index: 10241,
            delay: 2000
        });



}


//Bind DropDown with API
function bindselect(ajaxurl, ddidname, errordiv) {
    //alert(ddidname);
    var myjson = [];
    var errorflag = [];
    $.getJSON(ajaxurl, { format: "json" }).done(function (data) {
        myjson = data.data;
       
        errorflag = data.response[0].CODE;
        if (errorflag === "500") {
            $('#' + errordiv).html("<div class='text-center'>Server Side Error .<div>");
        }
        else if (errorflag === "200" & myjson === null) {
            $('#' + errordiv).html("<div class='text-center' style='color:#fff!important;'>Data not available .<div>");
        }
        else {
            if (data.data.length === 0) {
                $('#' + errordiv).css('background-color', 'rgba(244, 67, 54, 0.85)');
                $('#' + errordiv).fadeIn('slow');
                $('#' + errordiv).html("<i class='fa fa-info-circle'></i>&nbsp;&nbsp;<label id='lblerrorpage'> No data available in " + ddidname + " DropDown </label>");
                setTimeout(function () {
                    $('#' + errordiv).fadeOut('slow');
                }, 3000);

            }
            else {
                $('#' + ddidname).select2({
                    casesensitive: false,
                    tags: true,
                    data: data.data,
                    cache: true,
                    delay: 250,
                    closeOnSelect: true
                   
                });
              

            }
        }
    });
}


function bindselect1(ajaxurl, ddidname, errordiv) {
    var role = $("#ddexternalrole option:selected").val();
    $('#' + ddidname).empty();
    $("#divsl_EXT").show();
    $("#divsl1_EXT").hide();
    //get all role
    $.ajax({
        type: "GET",
        url: ajaxurl,
        dataType: "json",
        success: function (data) {
            var item = data.data[0];
            //alert(item.ROLE);
            var div_data = "<option value=''> NA </option>";
            $(div_data).appendTo('#' + ddidname);
            for (var i = 0; i < data.data.length; i++) {
                var item = data.data[i];
                div_data = "<option value=" + item.ROLE_ID + ">" + item.ROLE + "</option>";
                $(div_data).appendTo('#dd_assigned_under_role_EXT');

            }

        }
    });
    //alert(ddidname);
    var myjson = [];
    var errorflag = [];
    $.getJSON(ajaxurl, { format: "json" }).done(function (data) {
        myjson = data.data;
        $('#' + ddidname).empty();
        errorflag = data.response[0].CODE;
        if (errorflag === "500") {
            $('#' + errordiv).html("<div class='text-center'>Server Side Error .<div>");
        }
        else if (errorflag === "200" & myjson === null) {
            $('#' + errordiv).html("<div class='text-center' style='color:#fff!important;'>Data not available .<div>");
        }
        else {
            if (data.data.length === 0) {
                $('#' + errordiv).css('background-color', 'rgba(244, 67, 54, 0.85)');
                $('#' + errordiv).fadeIn('slow');
                $('#' + errordiv).html("<i class='fa fa-info-circle'></i>&nbsp;&nbsp;<label id='lblerrorpage'> No data available in " + ddidname + " DropDown </label>");
                setTimeout(function () {
                    $('#' + errordiv).fadeOut('slow');
                }, 3000);

            }
            else {
                var item = data.data[0];
                //alert(item.ROLE);
                $(div_data).appendTo('#' + ddidname);
                var div_data = "<option value=''> NA </option>";
           
                for (var i = 0; i < data.data.length; i++) {
                    var item = data.data[i];
                    div_data = "<option value=" + item.User_Guid + ">" + item.Name + "</option>";
                    $(div_data).appendTo('#dd_assigned_under_role_EXT');

                }



            }
        }
    });
}

//Bind Delete Record
function deletedatafromdatabel(datatabelname, dataid, spname, type, parametername) {

    if (datatabelname !== "" && dataid !== "" && spname !== "" && type !== "" && parametername !== "") {

        var obj = {};


        obj.dataid = dataid;
        obj.spname = spname;
        obj.type = type;
        obj.parametername = parametername;

        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            url: '../web_method/allwebmethod.aspx/delete_datatabel_record',
            data: JSON.stringify(obj),

            async: false,
            processData: false,
            success: function (response) {


                if (response.d === "1") {


                    sucess_msg("Data Deleted Successfully");
                }

                else {
                    error_msg(response.d);
                }


            },
            error: function () {
                error_msg(" Error Deleting data.Please try again!");
            }
        });
    }
    else {
        info_msg("Something Wrong !");
    }

}

//---------- image preview
function upload_imgprview(input, targetidname) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#' + targetidname).attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

//---------- query String
function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

//---------- Zoom Image
function open_imagecontrolmodal(a) {

    var modalImg = $("#viewer .img");

    modalImg.src = a.src;
    var imgezoom = a.src;

    var iv2 = $("#viewer").iviewer(
        {
            src: imgezoom
        });

    iv2.iviewer('loadImage', a.src);
    $('#imagecontrolmodal').modal('show');

}


function closeModal() {
    $('#imagecontrolmodal').modal('hide');
}

function sucess_msg(customerrormsg) {
    $('#commonerrorwrap').css('background-color', '#4CAF50');
    $("#commonerrorwrap").fadeIn('slow');
    $('#commonerrorwrap').html("<i class='fa fa-check-square'></i>&nbsp;&nbsp;<label id='lblerrorpage'>" + customerrormsg + "</label>");
    $('#commonerrorwrap').modal('hide');
    setTimeout(function () {
        $("#commonerrorwrap").fadeOut('slow');
    }, 2000);
}

function info_msg(customerrormsg) {
    $('#commonerrorwrap').css('background-color', '#00BCD4');
    $("#commonerrorwrap").fadeIn('slow');
    $('#commonerrorwrap').html("<i class='fa fa-info-circle'></i>&nbsp;&nbsp;<label id='lblerrorpage'>" + customerrormsg + "</label>");
    $('#commonerrorwrap').modal('hide');
    setTimeout(function () {
        $("#commonerrorwrap").fadeOut('slow');
    }, 2000);
}

function error_msg(customerrormsg) {
    $('#commonerrorwrap').css('background-color', '#F44336');
    $("#commonerrorwrap").fadeIn('slow');
    $('#commonerrorwrap').html("<i class='fa fa-window-close'></i>&nbsp;&nbsp;<label id='lblerrorpage'>" + customerrormsg + "</label>");
    $('#commonerrorwrap').modal('hide');
    setTimeout(function () {
        $("#commonerrorwrap").fadeOut('slow');
    }, 2000);
}

function valueupdown(oldvalue, updated, newvalue) {
    var updated1 = parseInt(updated);
    var oldvalue1 = parseInt(oldvalue);
    var newvalue1 = parseInt(newvalue);

    var diff = newvalue1 - oldvalue1;

    if (counterflag === "1") {

        if (updated1 !== oldvalue1) {
            //alert(updated + oldvalue);
            if (diff > 0) {
                $('#cvalue').html(newvalue + "&nbsp;<span class='text-success font-size-sm font-weight-normal'><i class='icon-arrow-up12'></i>&nbsp;(&nbsp;" + diff + "&nbsp;)&nbsp;&nbsp;</span>");
                alert(diff);

            }
            else if (diff === 0) {
                $('#cvalue').html(newvalue + "&nbsp;<span class='text-success font-size-sm font-weight-normal'>=&nbsp;(&nbsp;" + diff + "&nbsp;)&nbsp;&nbsp;</span>");

            }
            else if (diff < 0) {
                $('#cvalue').html(newvalue + "&nbsp;<span class='text-danger font-size-sm font-weight-normal'><i class='icon-arrow-down12'></i>&nbsp;(&nbsp;" + diff + "&nbsp;)&nbsp;&nbsp;</span>");
                alert(diff);
            }
            else {
                $('#cvalue').html(newvalue + "&nbsp;<span class='text-success font-size-sm font-weight-normal'><i class='icon-arrow-down12'></i>&nbsp;(&nbsp;" + diff + "&nbsp;)&nbsp;&nbsp;</span>");
            }
        }
        else {

            $('#cvalue').html(samedata);
        }

    }
    else {

        diff = "0";
        $('#cvalue').html(newvalue + "&nbsp;<span class='text-success font-size-sm font-weight-normal'>=&nbsp;(&nbsp;" + diff + "&nbsp;)&nbsp;&nbsp;</span>");
        counterflag = "1";
        samedata = newvalue + "&nbsp;<span class='text-success font-size-sm font-weight-normal'>=&nbsp;(&nbsp;" + diff + "&nbsp;)&nbsp;&nbsp;</span>";

    }



}
function apply_status_style(datatabelname) {


    $('#' + datatabelname + ' .statusflag').not('.statusheader').each(function () {
        var val = $(this).text();

        if (val === "NEW") {
            $(this).addClass("NEW");
        }
        else if (val === "ONSITECANCELAPPROVAL") {
            $(this).addClass("ONSITECANCELAPPROVAL");
        }
        else if (val === "ASSIGNED") {
            $(this).addClass("ASSIGNED");
        }
        else if (val === "ONHOLD") {
            $(this).addClass("ONHOLD");
        }
        else if (val === "INPROGRESS") {
            $(this).addClass("INPROGRESS");
        }
        else if (val === "COMPLETED") {
            $(this).addClass("COMPLETED");
        }
        else {
            $(this).addClass("");
        }
        $(this).addClass("text-center");
    });


}
/*=============Timeline bind code========================================*/

function timeline(divid, URL) {

    var myjson = [];
    var errorflag = [];
    $.getJSON(URL, { format: "json" }).done(function (data) {

        // Object split 
        myjson = data.data;
        errorflag = data.response[0].CODE;
        //   alert(errorflag);
        if (errorflag === "500") {
            $('#' + divid).html("<div class='text-center'>Server Side Error .<div>");
        }
        else if (errorflag === "200" & myjson === null) {
            $('#' + divid).html("<div class='text-center'>Timeline data not available .<div>");
        }
        else {
            
            var timelinewrap = "";
            for (var i = 0; i < data.data.length; i++) {
                var item = data.data[i];

                timelinewrap = timelinewrap + "<div class='list-feed-item'> <div class='arrow-left'></div>";
                timelinewrap = timelinewrap + "<div class='list_box'>";
                timelinewrap = timelinewrap + "<div class='font-size-sm mb-2' style='font-weight: 600;'> <i class='fa fa-calendar' aria-hidden='true'></i>&nbsp;" + item.CREATED_ON + "&nbsp;&nbsp;" + statusbadgenew(item.STATUS) + "</div>";
                if (item.REMARK !== "") {
                    timelinewrap = timelinewrap + "<p class='mb-1'><i class='fa fa-commenting-o' aria-hidden='true'></i>&nbsp;" + item.REMARK + "</p>";
                }
                timelinewrap = timelinewrap + "<span class='text-700' style='font-size:11px;'><i class='fa fa-user-o' aria-hidden='true'></i>&nbsp;" + item.NAME + "</span>";
                timelinewrap = timelinewrap + "</div>";
                timelinewrap = timelinewrap + "</div>";

            }
            $('#' + divid).html(timelinewrap);
        }
    });
}
/*=============Timeline bind code========================================*/

function leadtimeline(divid, URL) {

    var myjson = [];
    var errorflag = [];
    $.getJSON(URL, { format: "json" }).done(function (data) {

        // Object split 
        myjson = data.data;
        errorflag = data.response[0].CODE;
        //   alert(errorflag);
        if (errorflag === "500") {
            $('#' + divid).html("<div class='text-center'>Server Side Error .<div>");
        }
        else if (errorflag === "200" & myjson === null) {
            $('#' + divid).html("<div class='text-center'>Timeline data not available .<div>");
        }
        else {
            ;
            var timelinewrap = "";
            for (var i = 0; i < data.data.length; i++) {
                var item = data.data[i];

                timelinewrap = timelinewrap + "<div class='list-feed-item'> <div class='arrow-left'></div>";
                timelinewrap = timelinewrap + "<div class='list_box'>";
                timelinewrap = timelinewrap + "<div class='font-size-sm mb-2' style='font-weight: 600;'> <i class='fa fa-calendar' aria-hidden='true'></i>&nbsp;" + item.CREATED_ON + "&nbsp;&nbsp;" + statusbadgenew(item.STATUS) + "</div>";
                if (item.REMARK !== "") {
                    timelinewrap = timelinewrap + "<p class='mb-1'>&nbsp;" + item.REMARK + "</p>";
                }
                timelinewrap = timelinewrap + "<span class='text-700' style='font-size:11px;'><i class='fa fa-user-o' aria-hidden='true'></i>&nbsp;" + item.NAME + "</span>";
                timelinewrap = timelinewrap + "</div>";
                timelinewrap = timelinewrap + "</div>";

            }
            $('#' + divid).html(timelinewrap);
        }
    });
}
/*=============auditattempt bind code========================================*/
function auditattempttimeline(divid, URL) {
    var myjson1 = [];
    var errorflag1 = [];
    $.getJSON(URL, { format: "json" }).done(function (data) {

        // Object split 
        myjson1 = data.data;
        errorflag1 = data.response[0].CODE;
        var auditid = "";
        var dataflag = "0";
        var j = "0";
        //   alert(errorflag);
        if (errorflag1 === "500") {
            $('#' + divid).html("<div class='text-center'>Server Side Error .<div>");
        }
        else if (errorflag1 === "200" & myjson1 === null) {
            $('#' + divid).html("<div class='text-center'>Timeline data not available .<div>");
        }
        else {
            var timelinewrap = "";
            for (var i = 0; i < data.data.length; i++) {

                var item = data.data[i];

                if (auditid === item.AUDIT_ID) {


                    timelinewrap = timelinewrap + " <tr>";
                    timelinewrap = timelinewrap + "<td>" + item.PROMISEDATE + "</td>";
                    timelinewrap = timelinewrap + " <td>" + item.AUDITOR_NAME + "</td>";
                    timelinewrap = timelinewrap + " <td>" + item.ATTEMPT_STATUS + "</td>";
                    timelinewrap = timelinewrap + "<td>" + item.ACTION_DATE + "</td>";

                    timelinewrap = timelinewrap + "</tr>";


                    auditid = item.AUDIT_ID;
                }
                else {
                    j++;
                    timelinewrap = timelinewrap + "<table class='responsive table  table-bordered table-sm  table-hover table-striped' cellspacing='0' width='100%'>";
                    timelinewrap = timelinewrap + "<tr ><th class='thead' colspan='2'>Audit : " + j + "</th><th class='thead' colspan='2'>AUDIT CREATED BY :&nbsp;" + item.AUDIT_CREATED_BY + "&nbsp; </th></tr >";
                    timelinewrap = timelinewrap + "<tr><th class='thead' colspan='2'>AUDIT STATUS :&nbsp" + item.AUDIT_STATUS + "&nbsp;</th><th class='thead' colspan='2'>CREATED ON :&nbsp;" + item.CREATED_ON + "</th></tr >";

                    timelinewrap = timelinewrap + " <tr>";
                    timelinewrap = timelinewrap + "<th>PROMISE DATE</th>";
                    timelinewrap = timelinewrap + " <th>AUDITOR NAME</th>";
                    timelinewrap = timelinewrap + " <th>ATTEMPT STATUS</th>";
                    timelinewrap = timelinewrap + "<th>ACTION DATE</th>";

                    timelinewrap = timelinewrap + "</tr>";
                    dataflag = "0";
                    auditid = item.AUDIT_ID;
                    if (dataflag === "0") {

                        timelinewrap = timelinewrap + " <tr>";
                        timelinewrap = timelinewrap + "<td>" + item.PROMISEDATE + "</td>";
                        timelinewrap = timelinewrap + " <td>" + item.AUDITOR_NAME + "</td>";
                        timelinewrap = timelinewrap + " <td>" + item.ATTEMPT_STATUS + "</td>";
                        timelinewrap = timelinewrap + "<td>" + item.ACTION_DATE + "</td>";

                        timelinewrap = timelinewrap + "</tr>";
                        dataflag = "1";
                    }

                }
            }

            $('#' + divid).html(timelinewrap);


        }
    });
}

function validationemail(txtboxname) {
    var emailid = $('#' + txtboxname).val();
    if (emailid != "") {

    $.ajax({
        type: 'POST',
        headers: {
            'authorization': 'f35f201f-ff38-4cc6-8b09-81c2535f48e5'

        },
        contentType: "application/json; charset=utf-8",
        url: 'https://mailifier.io/api/v1/apps/single?email=' + emailid,
        //data: JSON.stringify(obj),
        async: false,
        processData: false,
        success: function (response) {
            setTimeout(function () {
                $.ajax({
                    type: 'GET',
                    headers: {
                        'authorization': 'f35f201f-ff38-4cc6-8b09-81c2535f48e5'

                    },
                    contentType: "application/json; charset=utf-8",
                    url: 'https://mailifier.io/api/v1/apps/single/' + response.data._id,
                    //data: JSON.stringify(obj),
                    async: false,
                    processData: false,
                    success: function (response) {

                        if (response.status == "0") {
                            error_msg("Pending");
                           
                            $('#' + txtboxname).css("border", "1px solid red");
                        }
                        else if (response.status == "1") {
                            sucess_msg("Valid Email Id");
                            $('#' + txtboxname).css("border", "1px solid #43c648");
                        }
                        else if (response.status == "2") {
                            error_msg("Unverifiable");
                            $('#' + txtboxname).css("border", "1px solid red");
                        }
                        else if (response.status == "3") {
                            sucess_msg("Disposable");
                            $('#' + txtboxname).css("border", "1px solid red");
                        }
                        else if (response.status == "4") {
                            error_msg("Invalid Email Id");
                            $('#' + txtboxname).css("border", "1px solid red");
                        }
                        else if (response.status == "5") {
                            error_msg("Unknown Email Id");
                            $('#' + txtboxname).css("border", "1px solid red");
                        }
                        else if (response.status == "6") {
                            error_msg("Bad Syntax Email Id");
                            $('#' + txtboxname).css("border", "1px solid red");
                        }
                        else {
                            error_msg("Something went wrong. Please try again!");
                            $('#' + txtboxname).css("border", "1px solid red");

                        }

                    },
                    error: function () {

                        error_msg("Something went wrong. Please try again!");
                    }
                });

            }, 1500);

        },
        error: function () {

            error_msg("Something went wrong. Please try again!");
        }
    });

    }
}

function allcharacterallow(txtboxname) {
    $.each(txtboxname, function (index, value) {
        $('#' + value).keypress(function (event) {
        
            var inputValue = event.which;
            // allow letters and whitespaces only.
            if (!(inputValue >= 65 && inputValue <= 90) && !(inputValue >= 97 && inputValue <= 122) && !(inputValue >= 48 && inputValue <= 57) && (inputValue !== 32 && inputValue !== 0)) {
                event.preventDefault();
            }
        });
    });
}
function validationremove(txtboxname) {
    $.each(txtboxname, function (index, value) {
        $('#' + value).keypress(function (event) {
            $('#' + txtboxname).css("background-color", "#ffffff");
           
        });
    });
}
function validationapply(txtboxname) {
    $.each(txtboxname, function (index, value) {
        if ($('#' + value).val() === '') {
            $('#' + value).css("background-color", "#F44336");
    }
    else {
            $('#' + value).css("background-color", "#ffffff");
        }
    });
}

function allalphaallow(txtboxname) {
    $.each(txtboxname, function (index, value) {
        $('#' + value).keypress(function (event) {
            var inputValue = event.which;
            // allow letters and whitespaces only.
            if (!(inputValue >= 65 && inputValue <= 90) && !(inputValue >= 97 && inputValue <= 122) && (inputValue >= 48 && inputValue <= 57) && (inputValue !== 32 && inputValue !== 0)) {
                event.preventDefault();
            }
        });
    });

}
function emailcharacter(txtboxname) {
    $.each(txtboxname, function (index, value) {
        $('#' + value).keypress(function (event) {
            var inputValue = event.which;
            // allow letters and whitespaces only.
            if ((inputValue >= 65 && inputValue <= 90) && !(inputValue >= 97 && inputValue <= 122) && !(inputValue >= 48 && inputValue <= 57) && (inputValue !== 64 && inputValue !== 44 && inputValue !== 45 && inputValue !== 47 && inputValue !== 46 && inputValue !== 0)) {
                event.preventDefault();
            }
        });
    });
}
$(document).keydown(function (event) {
    if (event.keyCode === 27) {
        closeModal(); return false;
    }
});
function addresscharacter(txtboxname) {
    $.each(txtboxname, function (index, value) {
        $('#' + value).keypress(function (event) {
            var inputValue = event.which;
            // allow letters and whitespaces only.
            if (!(inputValue >= 65 && inputValue <= 90) && !(inputValue >= 97 && inputValue <= 122) && !(inputValue >= 48 && inputValue <= 57) && (inputValue !== 32 && inputValue !== 44 && inputValue !== 45 && inputValue !== 47 && inputValue !== 0)) {
                event.preventDefault();
            }
        });
    });
}
function allcharacterallownotspace(txtboxname) {
    $.each(txtboxname, function (index, value) {
        $('#' + value).keypress(function (event) {
            var inputValue = event.which;
            // allow letters and whitespaces only.
            if (!(inputValue >= 65 && inputValue <= 90) && !(inputValue >= 97 && inputValue <= 122) && !(inputValue >= 48 && inputValue <= 57) && (inputValue === 32 && inputValue !== 0)) {
                event.preventDefault();
            }
        });
    });
}

function onlynumberallow(txtboxname) {

    $.each(txtboxname, function (index, value) {

        $('#' + value).keypress(function (event) {
            var inputValue = event.which;
            // allow letters and whitespaces only.
            if (inputValue > 31 && (inputValue < 48 || inputValue > 57)) {
                event.preventDefault();
            }
        });
    });

}

function onlynumberallowlass(txtboxname) {

    $.each(txtboxname, function (index, value) {

        $('.' + value).keypress(function (event) {
            var inputValue = event.which;
            // allow letters and whitespaces only.
            if (inputValue > 31 && (inputValue < 48 || inputValue > 57)) {
                event.preventDefault();
            }
        });
    });

}

function nodata(divid) {

    $('#' + divid).html("<div class='col-md-12 pt-3'> <div class='card' style='text-align: center;    border-radius: 10px;'> <div class='card-header header-elements-inline' style='background-color: #007dc4;'><h4 class='card-title' style='margin:0px auto;'><i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp;&nbsp;No data available </h4></div></div></div>");

}
function nodataerror(divid, msg) {

    $('#' + divid).html("<div class='col-md-12 pt-3'> <div class='card' style='text-align: center;    border-radius: 10px;'> <div class='card-header header-elements-inline' style='background-color: #007dc4;'><h4 class='card-title' style='margin:0px auto;'><i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp;&nbsp;" + msg + " </h4></div></div></div>");

}
function nodataverify(divid, mapdiv) {

    $('#' + divid).hide();
    $('#' + mapdiv).addClass("col-md-12");

}
function hideddtextbox(sourcediv, targetdiv) {

    if ($('#' + sourcediv).is(':checked')) {

        $('#' + targetdiv).show(500);

        if (sourcediv === "ContentPlaceHolder1_ckmandatory") { $("#errormandatory").html(""); }
    }
    else {
        $('#' + targetdiv).hide(500);
    }
}



function cleartextboxes() {
    $("input:text").val("");
    $("input:hidden").val("");
    $("textarea").val("");
}

function clearerrormsgonblur(errordiv) {
    $('#' + errordiv).html("");
}
function refreshandpopup(modelidname) {
    var pathname = $(location).attr('href');
    //var pathname = window.location.pathname;
    window.location.replace(pathname);
    $('#' + modelidname).modal('show');
}
function redirectadmin(status) {
    document.location.href = 'Admin_Dashboard_Details.aspx?status=' + status, true;
}
function redirectpage(status) {
    document.location.href = 'before_scheduling.aspx?status=' + status, true;
}
function redirectpage_a(status) {
    document.location.href = 'after_scheduling.aspx?status=' + status, true;
}

function redirectpageadminschedule(status) {
    document.location.href = 'before_scheduling.aspx?status=' + status, true;
}

function redirectpageadminschedule_a(status) {
    document.location.href = 'after_scheduling.aspx?status=' + status, true;
}
function redirectpageadminnew() {
    document.location.href = 'Verificationlist.aspx', true;
}


function redirectpageverify(status) {
    document.location.href = 'VerificationLog.aspx?status=' + status, true;
}

function redirectpageqc(status) {
    document.location.href = 'QC_Log.aspx?status=' + status, true;
}

function redirectpage_b(status) {
    document.location.href = 'Scheduling_Details.aspx?status=' + status, true;
}
function redirectverifypage(status) {
    document.location.href = 'VerificationLog.aspx?status=' + status, true;
}

function redirectsalespage(status) {
    document.location.href = 'Sales_Lead_details.aspx?status=' + status, true;
}



function timelinestatusbadgenew(STATUS) {

    if (STATUS.toUpperCase() === 'New'.toUpperCase()) { return "<span class='badge verify statusnew cursor-pointer'>New</span>"; }
    else if (STATUS.toUpperCase() === 'New'.toUpperCase()) { return "<span class='badge verify statusnew cursor-pointer'>New</span>"; }
    else if (STATUS.toUpperCase() === 'New lead'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>New lead</span>"; }
    else if (STATUS.toUpperCase() === 'Lead Assigned'.toUpperCase()) { return "<span class='badge verify statusqcassined cursor-pointer'>Lead Assigned</span>"; }
    else if (STATUS.toUpperCase() === 'Interested'.toUpperCase()) { return "<span class='badge verify statusassigned cursor-pointer'>Interested</span>"; }
    else if (STATUS.toUpperCase() === 'Interested'.toUpperCase()) { return "<span class='badge verify statusassigned cursor-pointer'>Interested</span>"; }
    else if (STATUS.toUpperCase() === 'Not Interested'.toUpperCase()) { return "<span class='badge verify statusnotagreed cursor-pointer'>Not Interested</span>"; }
    else if (STATUS.toUpperCase() === 'Auto Agreed'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>Auto Agreed</span>"; }
    else if (STATUS.toUpperCase() === 'Callback'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'>Callback</span>"; }
    else if (STATUS.toUpperCase() === 'Callback'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'>Callback</span>"; }
    else if (STATUS.toUpperCase() === 'Callback'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'>Callback</span>"; }
    else if (STATUS.toUpperCase() === 'audit Not created'.toUpperCase()) { return "<span class='badge statusauditnotcreated verify cursor-pointer'>audit Not created</span>"; }
    else if (STATUS.toUpperCase() === 'qc assined'.toUpperCase()) { return "<span class='badge verify statusqcassined cursor-pointer'>qc assined</span>"; }
    else if (STATUS.toUpperCase() === 'qc Assigned'.toUpperCase()) { return "<span class='badge verify statusqcassined cursor-pointer'>qc Assigned</span>"; }
    else if (STATUS.toUpperCase() === 'started'.toUpperCase()) { return "<span class='badge verify statusstarted cursor-pointer'>started</span>"; }
    else if (STATUS.toUpperCase() === 'qc done'.toUpperCase()) { return "<span class='badge verify statusqcdone cursor-pointer'>qc done</span>"; }
    else if (STATUS.toUpperCase() === 'Postpone by auditor'.toUpperCase()) { return "<span class='badge verify statuspostponebyauditor cursor-pointer'>Postpone by auditor</span>"; }
    else if (STATUS.toUpperCase() === 'accepted'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>accepted</span>"; }
    else if (STATUS.toUpperCase() === 'Agreed Postpone'.toUpperCase()) { return "<span class='badge verify statusAgreedPostpone cursor-pointer'>Agreed Postpone</span>"; }
    else if (STATUS.toUpperCase() === 'Postpone on call'.toUpperCase()) { return "<span class='badge verify statuspostponeoncall cursor-pointer'>Postpone on call</span>"; }
    else if (STATUS.toUpperCase() === 'Callback'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'>Callback</span>"; }
    else if (STATUS.toUpperCase() === 'Agreed'.toUpperCase()) { return "<span class='badge verify statusAgreed cursor-pointer'>Agreed</span>"; }
    else if (STATUS.toUpperCase() === 'Wrong number'.toUpperCase()) { return "<span class='badge verify statuswrongnumber cursor-pointer'>Wrong number</span>"; }
    else if (STATUS.toUpperCase() === 'payment approved'.toUpperCase()) { return "<span class='badge verify statuspaymentapproved cursor-pointer'>payment approved</span>"; }
    else if (STATUS.toUpperCase() === 'audit already done'.toUpperCase()) { return "<span class='badge verify statusauditalreadydone cursor-pointer'>audit already done</span>"; }
    else if (STATUS.toUpperCase() === 'Cancel by auditor'.toUpperCase()) { return "<span class='badge verify statusCancelbyauditor cursor-pointer'>Cancel by auditor</span>"; }
    else if (STATUS.toUpperCase() === 'Not reachable'.toUpperCase()) { return "<span class='badge verify statusNotreachable cursor-pointer'>Not reachable</span>"; }
    else if (STATUS.toUpperCase() === 'Not reachable'.toUpperCase()) { return "<span class='badge verify statusNotreachable cursor-pointer'>Not reachable</span>"; }
    else if (STATUS.toUpperCase() === 'ok to audit'.toUpperCase()) { return "<span class='badge verify statusoktoaudit cursor-pointer'>ok to audit</span>"; }
    else if (STATUS.toUpperCase() === 'Ringing'.toUpperCase()) { return "<span class='badge verify statusRinging cursor-pointer'>Ringing</span>"; }
    else if (STATUS.toUpperCase() === 'Ringing'.toUpperCase()) { return "<span class='badge verify statusRinging cursor-pointer'>Ringing</span>"; }
    else if (STATUS.toUpperCase() === 'upload report'.toUpperCase()) { return "<span class='badge verify statusuploadreport cursor-pointer'>upload report</span>"; }
    else if (STATUS.toUpperCase() === 'duplicate'.toUpperCase()) { return "<span class='badge verify statusduplicate cursor-pointer'>duplicate</span>"; }
    else if (STATUS.toUpperCase() === 'Not contactable'.toUpperCase()) { return "<span class='badge verify statusNotcontactable cursor-pointer'>Not contactable</span>"; }
    else if (STATUS.toUpperCase() === 'Cancel on call'.toUpperCase()) { return "<span class='badge verify statusCanceloncall cursor-pointer'>Cancel on call</span>"; }
    else if (STATUS.toUpperCase() === 'report generated'.toUpperCase()) { return "<span class='badge verify statusreportgenerated cursor-pointer'>report generated</span>"; }
    else if (STATUS.toUpperCase() === 'Postpone by site'.toUpperCase()) { return "<span class='badge verify statuspostponebysite cursor-pointer'>Postpone by site</span>"; }
    else if (STATUS.toUpperCase() === 'qc reject'.toUpperCase()) { return "<span class='badge verify statusqcreject cursor-pointer'>qc reject</span>"; }
    else if (STATUS.toUpperCase() === 'completed'.toUpperCase()) { return "<span class='badge verify statuscompleted cursor-pointer'>completed</span>"; }
    else if (STATUS.toUpperCase() === 'verification pick'.toUpperCase()) { return "<span class='badge verify statusverificationpick cursor-pointer'>verification pick</span>"; }
    else if (STATUS.toUpperCase() === 'Cancel by site'.toUpperCase()) { return "<span class='badge verify statusCancelbysite cursor-pointer'>Cancel by site</span>"; }
    else if (STATUS.toUpperCase() === 'payment reject'.toUpperCase()) { return "<span class='badge verify statuspaymentreject cursor-pointer'>payment reject</span>"; }
    else if (STATUS.toUpperCase() === 'pending approval'.toUpperCase()) { return "<span class='badge verify statuspendingapproval cursor-pointer'>pending approval</span>"; }
    else if (STATUS.toUpperCase() === 'shut down'.toUpperCase()) { return "<span class='badge verify statusshutdown cursor-pointer'>shut down</span>"; }
    else if (STATUS.toUpperCase() === 'Not Agreed'.toUpperCase()) { return "<span class='badge verify statusnotagreed cursor-pointer'>Not Agreed</span>"; }
    else if (STATUS.toUpperCase() === 'Cancel approval'.toUpperCase()) { return "<span class='badge verify statuscancelapproval cursor-pointer'>Cancel approval</span>"; }
    else if (STATUS.toUpperCase() === 'address Not found'.toUpperCase()) { return "<span class='badge verify statusaddressnotfound cursor-pointer'>address Not found</span>"; }
    else if (STATUS.toUpperCase() === 'Postpone approved'.toUpperCase()) { return "<span class='badge verify statuspostponeapproved cursor-pointer'>Postpone approved</span>"; }
    else if (STATUS.toUpperCase() === 'verification Assigned'.toUpperCase()) { return "<span class='badge verify statusverificationAssigned cursor-pointer'>verification Assigned</span>"; }
    else if (STATUS.toUpperCase() === 'qc approved'.toUpperCase()) { return "<span class='badge verify statusqcapporved cursor-pointer'>qc approved</span>"; }
    else if (STATUS.toUpperCase() === 'address Not found approved'.toUpperCase()) { return "<span class='badge verify statusaddressnotfoundapproved cursor-pointer'>address Not found approved</span>"; }
    else if (STATUS.toUpperCase() === 'Assigned'.toUpperCase()) { return "<span class='badge verify statusassigned cursor-pointer'>Assigned</span>"; }
    else if (STATUS.toUpperCase() === 'closed'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'>closed</span>"; }
    else if (STATUS.toUpperCase() === 'email'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>email</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>Meeting</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting_end'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>Meeting end</span>"; }

    else if (STATUS.toUpperCase() === 'got it done through fssai'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>got it done through fssai</span>"; }
    else if (STATUS.toUpperCase() === 'doesnt care'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor - pointer'>doesnt care</span>"; }
    else if (STATUS.toUpperCase() === 'doesnt believe mandate'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor - pointer'>doesnt believe mandate</span>"; }
    else if (STATUS.toUpperCase() === 'unresponsive'.toUpperCase()) { return "<span class='badge verify statuscallback cursor - pointer'>unresponsive</span>"; }
    else if (STATUS.toUpperCase() === 'Not enough business on z'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'>Not enough business on z</span>"; }
    else if (STATUS.toUpperCase() === 'Not on z anymore'.toUpperCase()) { return "<span class='badge verify statuscallback curso -pointer'>Not on z anymore</span>"; }
    else if (STATUS.toUpperCase() === 'already done'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>already done</span>"; }
    else if (STATUS.toUpperCase() === 'canNot afford'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>can Not afford</span>"; }
    else if (STATUS.toUpperCase() === 'revoke'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>revoke</span>"; }
    else if (STATUS.toUpperCase() === 'opt-in'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>opt-in</span>"; }
    else if (STATUS.toUpperCase() === 'Converted'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>Converted</span>"; }
    else if (STATUS.toUpperCase() === 'Converted'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>Converted</span>"; }
    else if (STATUS.toUpperCase() === 'Job'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>Job</span>"; }
    else if (STATUS.toUpperCase() === 'Future prospect'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>Future prospect</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting scheduled'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>Meeting scheduled</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting_scheduled'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>Meeting scheduled</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting start'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>Meeting start</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting_start'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>Meeting start</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting start'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>Meeting start</span>"; }

    else if (STATUS.toUpperCase() === 'Meeting Postpone by agent'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>Meeting Postpone by agent</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting_Postpone_by_agent'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>Meeting Postpone by agent</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting_Postpone_by_site'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>Meeting Postpone by site</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting_Cancel_by_site'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>Meeting Cancel by site</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting Postponed by site'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>Meeting Postpone by agent</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting done'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>Meeting done</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting end'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>Meeting end</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting Cancel by agent'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'>Meeting Cancel by agent</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting Cancel by site'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'>Meeting Cancel by site</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting_Cancel_by_agent'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'>Meeting Cancel by agent</span>"; }
    else if (STATUS.toUpperCase() === 'Existing client-amc'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'> Existing client amc</span>"; }
    else if (STATUS.toUpperCase() === 'Existing client'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'> Existing client</span>"; }
    else if (STATUS.toUpperCase() === 'Intro mail sent'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'> Intro mail sent</span>"; }
    else if (STATUS.toUpperCase() === 'Proposal sent'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'> Proposal sent</span>"; }
    else if (STATUS.toUpperCase() === 'Direct call'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'> Direct call</span>"; }
    else if (STATUS.toUpperCase() === 'Dead'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'> Dead</span>"; }
    else if (STATUS.toUpperCase() === 'Not Relevant'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'> Not Relevant</span>"; }
    else if (STATUS.toUpperCase() === 'Relevant'.toUpperCase()) { return "<span class='badge verify statusautoagreed  cursor-pointer'> Relevant</span>"; }
    else if (STATUS.toUpperCase() === 'Not contactable'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'> Not contactable</span>"; }
    else if (STATUS.toUpperCase() === 'Discussion done'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'> Discussion done</span>"; }
    else if (STATUS.toUpperCase() === 'Follow up done'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'> Follow up done</span>"; }
    else if (STATUS.toUpperCase() === 'Inbound call'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'> Inbound call</span>"; }
    else if (STATUS.toUpperCase() === 'Language barrier  '.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'> Language barrier</span>"; }
    else if (STATUS.toUpperCase() === 'Language barrier  '.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'> Language barrier</span>"; }
    else { return ''; }

}
function statusbadgenew(STATUS) {

    if (STATUS.toUpperCase() === 'New'.toUpperCase()) { return "<span class='badge verify statusnew cursor-pointer'>New</span>"; }
    else if (STATUS.toUpperCase() === 'New'.toUpperCase()) { return "<span class='badge verify statusnew cursor-pointer'>New</span>"; }
    else if (STATUS.toUpperCase() === 'New lead'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>New lead</span>"; }
    else if (STATUS.toUpperCase() === 'Lead Assigned'.toUpperCase()) { return "<span class='badge verify statusqcassined cursor-pointer'>Lead Assigned</span>"; }
    else if (STATUS.toUpperCase() === 'Assigned'.toUpperCase()) { return "<span class='badge verify statusqcassined cursor-pointer'>Assigned</span>"; }
    else if (STATUS.toUpperCase() === 'Interested'.toUpperCase()) { return "<span class='badge verify statusassigned cursor-pointer'>Interested</span>"; }
    else if (STATUS.toUpperCase() === 'Interested'.toUpperCase()) { return "<span class='badge verify statusassigned cursor-pointer'>Interested</span>"; }
    else if (STATUS.toUpperCase() === 'Not Interested'.toUpperCase()) { return "<span class='badge verify statusnotagreed cursor-pointer'>Not Interested</span>"; }
    else if (STATUS.toUpperCase() === 'Auto Agreed'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>Auto Agreed</span>"; }
    else if (STATUS.toUpperCase() === 'Callback'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'>Callback</span>"; }
    else if (STATUS.toUpperCase() === 'Callback'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'>Callback</span>"; }
    else if (STATUS.toUpperCase() === 'audit Not created'.toUpperCase()) { return "<span class='badge statusauditnotcreated verify cursor-pointer'>audit Not created</span>"; }
    else if (STATUS.toUpperCase() === 'qc assined'.toUpperCase()) { return "<span class='badge verify statusqcassined cursor-pointer'>qc assined</span>"; }
    else if (STATUS.toUpperCase() === 'qc Assigned'.toUpperCase()) { return "<span class='badge verify statusqcassined cursor-pointer'>qc Assigned</span>"; }
    else if (STATUS.toUpperCase() === 'started'.toUpperCase()) { return "<span class='badge verify statusstarted cursor-pointer'>started</span>"; }
    else if (STATUS.toUpperCase() === 'qc done'.toUpperCase()) { return "<span class='badge verify statusqcdone cursor-pointer'>qc done</span>"; }
    else if (STATUS.toUpperCase() === 'Postpone by auditor'.toUpperCase()) { return "<span class='badge verify statuspostponebyauditor cursor-pointer'>Postpone by auditor</span>"; }
    else if (STATUS.toUpperCase() === 'accepted'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>accepted</span>"; }
    else if (STATUS.toUpperCase() === 'Agreed Postpone'.toUpperCase()) { return "<span class='badge verify statusAgreedPostpone cursor-pointer'>Agreed Postpone</span>"; }
    else if (STATUS.toUpperCase() === 'Postpone on call'.toUpperCase()) { return "<span class='badge verify statuspostponeoncall cursor-pointer'>Postpone on call</span>"; }
    else if (STATUS.toUpperCase() === 'Callback'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'>Callback</span>"; }
    else if (STATUS.toUpperCase() === 'Follow up'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>Follow up</span>"; }
    else if (STATUS.toUpperCase() === 'Agreed'.toUpperCase()) { return "<span class='badge verify statusAgreed cursor-pointer'>Agreed</span>"; }
    else if (STATUS.toUpperCase() === 'Wrong number'.toUpperCase()) { return "<span class='badge verify statuswrongnumber cursor-pointer'>Wrong number</span>"; }
    else if (STATUS.toUpperCase() === 'payment approved'.toUpperCase()) { return "<span class='badge verify statuspaymentapproved cursor-pointer'>payment approved</span>"; }
    else if (STATUS.toUpperCase() === 'audit already done'.toUpperCase()) { return "<span class='badge verify statusauditalreadydone cursor-pointer'>audit already done</span>"; }
    else if (STATUS.toUpperCase() === 'Cancel by auditor'.toUpperCase()) { return "<span class='badge verify statusCancelbyauditor cursor-pointer'>Cancel by auditor</span>"; }
    else if (STATUS.toUpperCase() === 'Not reachable'.toUpperCase()) { return "<span class='badge verify statusNotreachable cursor-pointer'>Not reachable</span>"; }
    else if (STATUS.toUpperCase() === 'Not reachable'.toUpperCase()) { return "<span class='badge verify statusNotreachable cursor-pointer'>Not reachable</span>"; }
    else if (STATUS.toUpperCase() === 'ok to audit'.toUpperCase()) { return "<span class='badge verify statusoktoaudit cursor-pointer'>ok to audit</span>"; }
    else if (STATUS.toUpperCase() === 'Ringing'.toUpperCase()) { return "<span class='badge verify statusRinging cursor-pointer'>Ringing</span>"; }
    else if (STATUS.toUpperCase() === 'Ringing'.toUpperCase()) { return "<span class='badge verify statusRinging cursor-pointer'>Ringing</span>"; }
    else if (STATUS.toUpperCase() === 'upload report'.toUpperCase()) { return "<span class='badge verify statusuploadreport cursor-pointer'>upload report</span>"; }
    else if (STATUS.toUpperCase() === 'duplicate'.toUpperCase()) { return "<span class='badge verify statusduplicate cursor-pointer'>duplicate</span>"; }
    else if (STATUS.toUpperCase() === 'Not contactable'.toUpperCase()) { return "<span class='badge verify statusNotcontactable cursor-pointer'>Not contactable</span>"; }
    else if (STATUS.toUpperCase() === 'Cancel on call'.toUpperCase()) { return "<span class='badge verify statusCanceloncall cursor-pointer'>Cancel on call</span>"; }
    else if (STATUS.toUpperCase() === 'report generated'.toUpperCase()) { return "<span class='badge verify statusreportgenerated cursor-pointer'>report generated</span>"; }
    else if (STATUS.toUpperCase() === 'Postpone by site'.toUpperCase()) { return "<span class='badge verify statuspostponebysite cursor-pointer'>Postpone by site</span>"; }
    else if (STATUS.toUpperCase() === 'qc reject'.toUpperCase()) { return "<span class='badge verify statusqcreject cursor-pointer'>qc reject</span>"; }
    else if (STATUS.toUpperCase() === 'completed'.toUpperCase()) { return "<span class='badge verify statuscompleted cursor-pointer'>completed</span>"; }
    else if (STATUS.toUpperCase() === 'verification pick'.toUpperCase()) { return "<span class='badge verify statusverificationpick cursor-pointer'>verification pick</span>"; }
    else if (STATUS.toUpperCase() === 'Cancel by site'.toUpperCase()) { return "<span class='badge verify statusCancelbysite cursor-pointer'>Cancel by site</span>"; }
    else if (STATUS.toUpperCase() === 'payment reject'.toUpperCase()) { return "<span class='badge verify statuspaymentreject cursor-pointer'>payment reject</span>"; }
    else if (STATUS.toUpperCase() === 'pending approval'.toUpperCase()) { return "<span class='badge verify statuspendingapproval cursor-pointer'>pending approval</span>"; }
    else if (STATUS.toUpperCase() === 'shut down'.toUpperCase()) { return "<span class='badge verify statusshutdown cursor-pointer'>shut down</span>"; }
    else if (STATUS.toUpperCase() === 'Not Agreed'.toUpperCase()) { return "<span class='badge verify statusnotagreed cursor-pointer'>Not Agreed</span>"; }
    else if (STATUS.toUpperCase() === 'Cancel approval'.toUpperCase()) { return "<span class='badge verify statuscancelapproval cursor-pointer'>Cancel approval</span>"; }
    else if (STATUS.toUpperCase() === 'address Not found'.toUpperCase()) { return "<span class='badge verify statusaddressnotfound cursor-pointer'>address Not found</span>"; }
    else if (STATUS.toUpperCase() === 'Postpone approved'.toUpperCase()) { return "<span class='badge verify statuspostponeapproved cursor-pointer'>Postpone approved</span>"; }
    else if (STATUS.toUpperCase() === 'verification Assigned'.toUpperCase()) { return "<span class='badge verify statusverificationAssigned cursor-pointer'>verification Assigned</span>"; }
    else if (STATUS.toUpperCase() === 'qc approved'.toUpperCase()) { return "<span class='badge verify statusqcapporved cursor-pointer'>qc approved</span>"; }
    else if (STATUS.toUpperCase() === 'address Not found approved'.toUpperCase()) { return "<span class='badge verify statusaddressnotfoundapproved cursor-pointer'>address Not found approved</span>"; }
    else if (STATUS.toUpperCase() === 'Assigned'.toUpperCase()) { return "<span class='badge verify statusassigned cursor-pointer'>Assigned</span>"; }
    else if (STATUS.toUpperCase() === 'closed'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'>closed</span>"; }
    else if (STATUS.toUpperCase() === 'email'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>email</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>Meeting</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting_end'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>Meeting end</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting_end'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>Meeting end</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting end'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>Meeting end</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting_start'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>Meeting start</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting_scheduled'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>Meeting scheduled</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting scheduled'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>Meeting scheduled</span>"; }
    else if (STATUS.toUpperCase() === 'got it done through fssai'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>got it done through fssai</span>"; }
    else if (STATUS.toUpperCase() === 'doesnt care'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor - pointer'>doesnt care</span>"; }
    else if (STATUS.toUpperCase() === 'doesnt believe mandate'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor - pointer'>doesnt believe mandate</span>"; }
    else if (STATUS.toUpperCase() === 'unresponsive'.toUpperCase()) { return "<span class='badge verify statuscallback cursor - pointer'>unresponsive</span>"; }
    else if (STATUS.toUpperCase() === 'Not enough business on z'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'>Not enough business on z</span>"; }
    else if (STATUS.toUpperCase() === 'Not on z anymore'.toUpperCase()) { return "<span class='badge verify statuscallback curso -pointer'>Not on z anymore</span>"; }
    else if (STATUS.toUpperCase() === 'already done'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>already done</span>"; }
    else if (STATUS.toUpperCase() === 'canNot afford'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>can Not afford</span>"; }
    else if (STATUS.toUpperCase() === 'revoke'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>revoke</span>"; }
    else if (STATUS.toUpperCase() === 'opt-in'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>opt-in</span>"; }
    else if (STATUS.toUpperCase() === 'Converted'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>Converted</span>"; }
    else if (STATUS.toUpperCase() === 'Converted'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>Converted</span>"; }
    else if (STATUS.toUpperCase() === 'Job'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>Job</span>"; }
    else if (STATUS.toUpperCase() === 'Future prospect'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>Future prospect</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting scheduled'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>Meeting scheduled</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting Postponed by site'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>Meeting Postpone by site</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting Cancel by agent'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'>Meeting Cancel by agent</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting Cancel by site'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'>Meeting Cancel by site</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting start'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>Meeting start</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting start'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>Meeting start</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting_start'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>Meeting start</span>"; }

    else if (STATUS.toUpperCase() === 'Meeting_Postpone_by_agent'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>Meeting Postpone by agent</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting_Postpone_by_site'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>Meeting Postpone by site</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting_Cancel_by_site'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'>Meeting Cancel by site</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting_Cancel_by_agent'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'>Meeting Cancel by agent</span>"; }
    else if (STATUS.toUpperCase() === 'Existing client-amc'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'> Existing client amc</span>"; }
    else if (STATUS.toUpperCase() === 'Existing client'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'> Existing client</span>"; }
    else if (STATUS.toUpperCase() === 'Intro mail sent'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'> Intro mail sent</span>"; }
    else if (STATUS.toUpperCase() === 'Proposal sent'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'> Proposal sent</span>"; }
    else if (STATUS.toUpperCase() === 'Direct call'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'> Direct call</span>"; }
    else if (STATUS.toUpperCase() === 'Dead'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'> Dead</span>"; }
    else if (STATUS.toUpperCase() === 'Not Relevant'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'> Not Relevant</span>"; }
    else if (STATUS.toUpperCase() === 'Relevant'.toUpperCase()) { return "<span class='badge verify statusautoagreed  cursor-pointer'> Relevant</span>"; }
    else if (STATUS.toUpperCase() === 'Not contactable'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'> Not contactable</span>"; }
    else if (STATUS.toUpperCase() === 'Language barrier '.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'> Language barrier</span>"; }
    else if (STATUS.toUpperCase() === 'Language barrier  '.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'> Language barrier</span>"; }
    else if (STATUS.toUpperCase() === 'Callback'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'>Callback</span>"; }
    else if (STATUS.toUpperCase() === 'Discussion done'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'> Discussion done</span>"; }
    else if (STATUS.toUpperCase() === 'Follow up done'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'> Follow up done</span>"; }
    else if (STATUS.toUpperCase() === 'Inbound call'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'> Inbound call</span>"; }

    else if (STATUS.toUpperCase() === 'Proposal revised'.toUpperCase()) { return "<span class='badge verify statusaccepted     cursor-pointer'>Proposal revised</span>"; }
    else if (STATUS.toUpperCase() === 'Proposal draft'.toUpperCase()) { return "<span class='badge verify statusnew cursor-pointer'>Proposal draft</span>"; }
    else if (STATUS.toUpperCase() === 'Proposal lost'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'>Proposal lost</span>"; }
    else if (STATUS.toUpperCase() === 'Proposal Converted'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>Proposal Converted</span>"; }
    else if (STATUS.toUpperCase() === 'Proposal sent'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>Proposal sent</span>"; }
    else if (STATUS.toUpperCase() === 'Future prospect'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>Future prospect</span>"; }

    else { return ''; }

}


function statusbadgenew2(STATUS) {

    if (STATUS.toUpperCase() === 'New'.toUpperCase()) { return "<span class=' font_blue cursor-pointer'>New</span>"; }
    else if (STATUS.toUpperCase() === 'New'.toUpperCase()) { return "<span class=' font_blue cursor-pointer'>New</span>"; }
    else if (STATUS.toUpperCase() === 'New lead'.toUpperCase()) { return "<span class='font_blue  cursor-pointer'>New lead</span>"; }
    else if (STATUS.toUpperCase() === 'Follow up'.toUpperCase()) { return "<span class='font_orange  cursor-pointer'>Follow up</span>"; }
    else if (STATUS.toUpperCase() === 'Lead Assigned'.toUpperCase()) { return "<span class='font_skyblue cursor-pointer'>Lead Assigned</span>"; }
    else if (STATUS.toUpperCase() === 'Interested'.toUpperCase()) { return "<span class='font_skyblue cursor-pointer'>Interested</span>"; }
    else if (STATUS.toUpperCase() === 'Interested'.toUpperCase()) { return "<span class='font_skyblue cursor-pointer'>Interested</span>"; }
    else if (STATUS.toUpperCase() === 'Not Interested'.toUpperCase()) { return "<span class='font_red cursor-pointer'>Not Interested</span>"; }
    else if (STATUS.toUpperCase() === 'Auto Agreed'.toUpperCase()) { return "<span class='font_green cursor-pointer'>Auto Agreed</span>"; }
    else if (STATUS.toUpperCase() === 'Callback'.toUpperCase()) { return "<span class='font_orange cursor-pointer'>Callback</span>"; }
    else if (STATUS.toUpperCase() === 'Callback'.toUpperCase()) { return "<span class='font_orange cursor-pointer'>Callback</span>"; }
    else if (STATUS.toUpperCase() === 'audit Not created'.toUpperCase()) { return "<span class='badge statusauditnotcreated verify cursor-pointer'>audit Not created</span>"; }
    else if (STATUS.toUpperCase() === 'qc assined'.toUpperCase()) { return "<span class='badge verify statusqcassined cursor-pointer'>qc assined</span>"; }
    else if (STATUS.toUpperCase() === 'qc Assigned'.toUpperCase()) { return "<span class='badge verify statusqcassined cursor-pointer'>qc Assigned</span>"; }
    else if (STATUS.toUpperCase() === 'started'.toUpperCase()) { return "<span class='badge verify statusstarted cursor-pointer'>started</span>"; }
    else if (STATUS.toUpperCase() === 'qc done'.toUpperCase()) { return "<span class='badge verify statusqcdone cursor-pointer'>qc done</span>"; }
    else if (STATUS.toUpperCase() === 'Proposal sent'.toUpperCase()) { return "<span class='font_green cursor-pointer'>Proposal sent</span>"; }
    else if (STATUS.toUpperCase() === 'Postpone by auditor'.toUpperCase()) { return "<span class='font_orange cursor-pointer'>Postpone by auditor</span>"; }
    else if (STATUS.toUpperCase() === 'accepted'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>accepted</span>"; }
    else if (STATUS.toUpperCase() === 'Agreed Postpone'.toUpperCase()) { return "<span class='badge verify statusAgreedPostpone cursor-pointer'>Agreed Postpone</span>"; }
    else if (STATUS.toUpperCase() === 'Postpone on call'.toUpperCase()) { return "<span class='font_orange cursor-pointer'>Postpone on call</span>"; }
    else if (STATUS.toUpperCase() === 'Callback'.toUpperCase()) { return "<span class='font_orange cursor-pointer'>Callback</span>"; }
    else if (STATUS.toUpperCase() === 'Agreed'.toUpperCase()) { return "<span class='badge verify statusAgreed cursor-pointer'>Agreed</span>"; }
    else if (STATUS.toUpperCase() === 'Wrong number'.toUpperCase()) { return "<span class='font_red cursor-pointer'>Wrong number</span>"; }
    else if (STATUS.toUpperCase() === 'payment approved'.toUpperCase()) { return "<span class='badge verify statuspaymentapproved cursor-pointer'>payment approved</span>"; }
    else if (STATUS.toUpperCase() === 'audit already done'.toUpperCase()) { return "<span class='font_orange cursor-pointer'>audit already done</span>"; }
    else if (STATUS.toUpperCase() === 'Cancel by auditor'.toUpperCase()) { return "<span class='font_orange cursor-pointer'>Cancel by auditor</span>"; }
    else if (STATUS.toUpperCase() === 'Not reachable'.toUpperCase()) { return "<span class='font_red cursor-pointer'>Not reachable</span>"; }
    else if (STATUS.toUpperCase() === 'Not reachable'.toUpperCase()) { return "<span class='font_red cursor-pointer'>Not reachable</span>"; }
    else if (STATUS.toUpperCase() === 'ok to audit'.toUpperCase()) { return "<span class='badge verify statusoktoaudit cursor-pointer'>ok to audit</span>"; }
    else if (STATUS.toUpperCase() === 'Ringing'.toUpperCase()) { return "<span class='font_orange'>Ringing</span>"; }
    else if (STATUS.toUpperCase() === 'Ringing'.toUpperCase()) { return "<span class='font_orange cursor-pointer'>Ringing</span>"; }
    else if (STATUS.toUpperCase() === 'upload report'.toUpperCase()) { return "<span class='font_orange cursor-pointer'>upload report</span>"; }
    else if (STATUS.toUpperCase() === 'duplicate'.toUpperCase()) { return "<span class='badge verify statusduplicate cursor-pointer'>duplicate</span>"; }
    else if (STATUS.toUpperCase() === 'Not contactable'.toUpperCase()) { return "<span class='font_red cursor-pointer'>Not contactable</span>"; }
    else if (STATUS.toUpperCase() === 'Cancel on call'.toUpperCase()) { return "<span class='font_orange cursor-pointer'>Cancel on call</span>"; }
    else if (STATUS.toUpperCase() === 'report generated'.toUpperCase()) { return "<span class='font_green cursor-pointer'>report generated</span>"; }
    else if (STATUS.toUpperCase() === 'Postpone by site'.toUpperCase()) { return "<span class='font_orange cursor-pointer'>Postpone by site</span>"; }
    else if (STATUS.toUpperCase() === 'qc reject'.toUpperCase()) { return "<span class='badge verify statusqcreject cursor-pointer'>qc reject</span>"; }
    else if (STATUS.toUpperCase() === 'completed'.toUpperCase()) { return "<span class='badge verify statuscompleted cursor-pointer'>completed</span>"; }
    else if (STATUS.toUpperCase() === 'verification pick'.toUpperCase()) { return "<span class='badge verify statusverificationpick cursor-pointer'>verification pick</span>"; }
    else if (STATUS.toUpperCase() === 'Cancel by site'.toUpperCase()) { return "<span class='font_red cursor-pointer'>Cancel by site</span>"; }
    else if (STATUS.toUpperCase() === 'payment reject'.toUpperCase()) { return "<span class='badge verify statuspaymentreject cursor-pointer'>payment reject</span>"; }
    else if (STATUS.toUpperCase() === 'pending approval'.toUpperCase()) { return "<span class='badge verify statuspendingapproval cursor-pointer'>pending approval</span>"; }
    else if (STATUS.toUpperCase() === 'shut down'.toUpperCase()) { return "<span class='font_red cursor-pointer'>shut down</span>"; }
    else if (STATUS.toUpperCase() === 'Not Agreed'.toUpperCase()) { return "<span class='font_orange cursor-pointer'>Not Agreed</span>"; }
    else if (STATUS.toUpperCase() === 'Cancel approval'.toUpperCase()) { return "<span class='badge verify statuscancelapproval cursor-pointer'>Cancel approval</span>"; }
    else if (STATUS.toUpperCase() === 'address Not found'.toUpperCase()) { return "<span class='badge verify statusaddressnotfound cursor-pointer'>address Not found</span>"; }
    else if (STATUS.toUpperCase() === 'Postpone approved'.toUpperCase()) { return "<span class='badge verify statuspostponeapproved cursor-pointer'>Postpone approved</span>"; }
    else if (STATUS.toUpperCase() === 'verification Assigned'.toUpperCase()) { return "<span class='badge verify statusverificationAssigned cursor-pointer'>verification Assigned</span>"; }
    else if (STATUS.toUpperCase() === 'qc approved'.toUpperCase()) { return "<span class='badge verify statusqcapporved cursor-pointer'>qc approved</span>"; }
    else if (STATUS.toUpperCase() === 'address Not found approved'.toUpperCase()) { return "<span class='badge verify statusaddressnotfoundapproved cursor-pointer'>address Not found approved</span>"; }
    else if (STATUS.toUpperCase() === 'Assigned'.toUpperCase()) { return "<span class='font_orange cursor-pointer'>Assigned</span>"; }
    else if (STATUS.toUpperCase() === 'closed'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'>closed</span>"; }
    else if (STATUS.toUpperCase() === 'email'.toUpperCase()) { return "<span class='font_orange cursor-pointer'>email</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting'.toUpperCase()) { return "<span class='font_green cursor-pointer'>Meeting</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting_end'.toUpperCase()) { return "<span class='font_green cursor-pointer'>Meeting end</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting_end'.toUpperCase()) { return "<span class='font_green cursor-pointer'>Meeting end</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting end'.toUpperCase()) { return "<span class='font_green cursor-pointer'>Meeting end</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting_start'.toUpperCase()) { return "<span class='font_green cursor-pointer'>Meeting start</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting_scheduled'.toUpperCase()) { return "<span class='font_green cursor-pointer'>Meeting scheduled</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting scheduled'.toUpperCase()) { return "<span class='font_green cursor-pointer'>Meeting scheduled</span>"; }
    else if (STATUS.toUpperCase() === 'got it done through fssai'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>got it done through fssai</span>"; }
    else if (STATUS.toUpperCase() === 'doesnt care'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor - pointer'>doesnt care</span>"; }
    else if (STATUS.toUpperCase() === 'doesnt believe mandate'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor - pointer'>doesnt believe mandate</span>"; }
    else if (STATUS.toUpperCase() === 'unresponsive'.toUpperCase()) { return "<span class='badge verify statuscallback cursor - pointer'>unresponsive</span>"; }
    else if (STATUS.toUpperCase() === 'Not enough business on z'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'>Not enough business on z</span>"; }
    else if (STATUS.toUpperCase() === 'Not on z anymore'.toUpperCase()) { return "<span class='badge verify statuscallback curso -pointer'>Not on z anymore</span>"; }
    else if (STATUS.toUpperCase() === 'already done'.toUpperCase()) { return "<span class='font_orange cursor-pointer'>already done</span>"; }
    else if (STATUS.toUpperCase() === 'canNot afford'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>can Not afford</span>"; }
    else if (STATUS.toUpperCase() === 'revoke'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>revoke</span>"; }
    else if (STATUS.toUpperCase() === 'opt-in'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>opt-in</span>"; }
    else if (STATUS.toUpperCase() === 'Converted'.toUpperCase()) { return "<span class='font_green cursor-pointer'>Converted</span>"; }
    else if (STATUS.toUpperCase() === 'Converted'.toUpperCase()) { return "<span class='font_green cursor-pointer'>Converted</span>"; }
    else if (STATUS.toUpperCase() === 'Job'.toUpperCase()) { return "<span class='font_red cursor-pointer'>Job</span>"; }
    else if (STATUS.toUpperCase() === 'Future prospect'.toUpperCase()) { return "<span class='font_green cursor-pointer'>Future prospect</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting scheduled'.toUpperCase()) { return "<span class='font_green cursor-pointer'>Meeting scheduled</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting Postponed by site'.toUpperCase()) { return "<span class='font_orange cursor-pointer'>Meeting Postpone by site</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting Cancel by agent'.toUpperCase()) { return "<span class='font_red cursor-pointer'>Meeting Cancel by agent</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting Cancel by site'.toUpperCase()) { return "<span class='font_red cursor-pointer'>Meeting Cancel by site</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting start'.toUpperCase()) { return "<span class='font_green cursor-pointer'>Meeting start</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting start'.toUpperCase()) { return "<span class='font_green cursor-pointer'>Meeting start</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting_start'.toUpperCase()) { return "<span class='font_green cursor-pointer'>Meeting start</span>"; }

    else if (STATUS.toUpperCase() === 'Meeting_Postpone_by_agent'.toUpperCase()) { return "<span class='font_orange cursor-pointer'>Meeting Postpone by agent</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting_Postpone_by_site'.toUpperCase()) { return "<span class='font_orange cursor-pointer'>Meeting Postpone by site</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting_Cancel_by_site'.toUpperCase()) { return "<span class='font_red cursor-pointer'>Meeting Cancel by site</span>"; }
    else if (STATUS.toUpperCase() === 'Meeting_Cancel_by_agent'.toUpperCase()) { return "<span class='font_red cursor-pointer'>Meeting Cancel by agent</span>"; }
    else if (STATUS.toUpperCase() === 'Existing client-amc'.toUpperCase()) { return "<span class='font_orange cursor-pointer'> Existing client amc</span>"; }
    else if (STATUS.toUpperCase() === 'Existing client'.toUpperCase()) { return "<span class='font_orange cursor-pointer'> Existing client</span>"; }
    else if (STATUS.toUpperCase() === 'Intro mail sent'.toUpperCase()) { return "<span class='font_orange cursor-pointer'> Intro mail sent</span>"; }
    else if (STATUS.toUpperCase() === 'Proposal sent'.toUpperCase()) { return "<span class='font_green cursor-pointer'> Proposal sent</span>"; }
    else if (STATUS.toUpperCase() === 'Direct call'.toUpperCase()) { return "<span class='font_green cursor-pointer'> Direct call</span>"; }
    else if (STATUS.toUpperCase() === 'Dead'.toUpperCase()) { return "<span class='font_red cursor-pointer'> Dead</span>"; }
    else if (STATUS.toUpperCase() === 'Not Relevant'.toUpperCase()) { return "<span class='font_red cursor-pointer'> Not Relevant</span>"; }
    else if (STATUS.toUpperCase() === 'Relevant'.toUpperCase()) { return "<span class='font_green cursor-pointer'> Relevant</span>"; }
    else if (STATUS.toUpperCase() === 'Not contactable'.toUpperCase()) { return "<span class='font_red cursor-pointer'> Not contactable</span>"; }
    else if (STATUS.toUpperCase() === 'Language barrier '.toUpperCase()) { return "<span class='font_red cursor-pointer'> Language barrier</span>"; }
    else if (STATUS.toUpperCase() === 'Language barrier  '.toUpperCase()) { return "<span class='font_red cursor-pointer'> Language barrier</span>"; }

    else if (STATUS.toUpperCase() === 'Proposal revised'.toUpperCase()) { return "<span class='font_orange cursor-pointer'>Proposal revised</span>"; }
    else if (STATUS.toUpperCase() === 'Proposal draft'.toUpperCase()) { return "<span class='font_orange cursor-pointer'>Proposal draft</span>"; }
    else if (STATUS.toUpperCase() === 'Proposal lost'.toUpperCase()) { return "<span class='font_red cursor-pointer'>Proposal lost</span>"; }
    else if (STATUS.toUpperCase() === 'Proposal Converted'.toUpperCase()) { return "<span class='font_green cursor-pointer'>Proposal Converted</span>"; }
    else if (STATUS.toUpperCase() === 'Proposal sent'.toUpperCase()) { return "<span class='font_green cursor-pointer'>Proposal sent</span>"; }
    else if (STATUS.toUpperCase() === 'Discussion done'.toUpperCase()) { return "<span class='font_orange cursor-pointer'> Discussion done</span>"; }
    else if (STATUS.toUpperCase() === 'Follow up done'.toUpperCase()) { return "<span class='font_orange cursor-pointer'> Follow up done</span>"; }
    else if (STATUS.toUpperCase() === 'Inbound call'.toUpperCase()) { return "<span class='font_orange cursor-pointer'> Inbound call</span>"; }
    else if (STATUS.toUpperCase() === 'Assigned'.toUpperCase()) { return "<span class='font_skyblue cursor-pointer'> Assigned</span>"; }
    else { return ''; }

}


function timelinestatusbadgenew1(STATUS) {


    if (STATUS.toUpperCase() === 'New'.toUpperCase()) { return "<div class='mid_ico blue'><i class='fa fa-file'></i></div>"; }
    else if (STATUS.toUpperCase() === 'New'.toUpperCase()) { return "<div class='mid_ico blue'><i class='fa fa-file'></i></div>"; }
    else if (STATUS.toUpperCase() === 'New'.toUpperCase()) { return "<div class='mid_ico blue'><i class='fa fa-file-o'></i></div>"; }
    else if (STATUS.toUpperCase() === 'New lead'.toUpperCase()) { return "<div class='mid_ico blue'> <i class='fa fa-file'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Follow up'.toUpperCase()) { return "<div class='mid_ico orange'> <i class='fa fa-arrow-up'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Lead Assigned'.toUpperCase()) { return "<div class='mid_ico skyblue'> <i class='fa fa-file'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Assigned'.toUpperCase()) { return "<div class='mid_ico skyblue'> <i class='fa fa-user-plus'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Interested'.toUpperCase()) { return "<div class='mid_ico skyblue'><i class='fa fa-check'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Interested'.toUpperCase()) { return "<div class='mid_ico skyblue'><i class='fa fa-check'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Not Interested'.toUpperCase()) { return "<div class='mid_ico red'><i class='fa fa-times'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Not Interested'.toUpperCase()) { return "<div class='mid_ico red'><i class='fa fa-times'></i></div>"; }

    else if (STATUS.toUpperCase() === 'Auto Agreed'.toUpperCase()) { return "<div class='mid_ico green'><i class='fa fa-check'></i>"; }
    else if (STATUS.toUpperCase() === 'Callback'.toUpperCase()) { return "<div class='mid_ico orange'><i class='fa fa-phone'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Callback'.toUpperCase()) { return "<div class='mid_ico orange'><i class='fa fa-check'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Callback'.toUpperCase()) { return "<div class='mid_ico orange'><i class='fa fa-check'></i></div>"; }
    else if (STATUS.toUpperCase() === 'audit Not created'.toUpperCase()) { return "<div class='mid_ico skyblue'><i class='fa fa-check'></i></div>"; }
    else if (STATUS.toUpperCase() === 'qc assined'.toUpperCase()) { return "<div class='mid_ico skyblue'><i class='fa fa-check'></i></div>"; }
    else if (STATUS.toUpperCase() === 'qc Assigned'.toUpperCase()) { return "<div class='mid_ico skyblue'><i class='fa fa-check'></i></div>"; }
    else if (STATUS.toUpperCase() === 'started'.toUpperCase()) { return "<div class='mid_ico skyblue'><i class='fa fa-check'></i></div>"; }
    else if (STATUS.toUpperCase() === 'qc done'.toUpperCase()) { return "<div class='mid_ico skyblue'><i class='fa fa-check'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Postpone by auditor'.toUpperCase()) { return "<div class='mid_ico orange'><i class='fa fa-share'></i></div>"; }
    else if (STATUS.toUpperCase() === 'accepted'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>accepted</span>"; }
    else if (STATUS.toUpperCase() === 'Agreed Postpone'.toUpperCase()) { return "<div class='mid_ico orange'><i class='fa fa-share'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Postpone on call'.toUpperCase()) { return "<div class='mid_ico orange'><i class='fa fa-phone'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Callback'.toUpperCase()) { return "<div class='mid_ico orange'><i class='fa fa-phone'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Agreed'.toUpperCase()) { return "<div class='mid_ico green'><i class='fa fa-check'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Wrong number'.toUpperCase()) { return "<div class='mid_ico red'><i class='fa fa-ban'></i></div>"; }
    else if (STATUS.toUpperCase() === 'payment approved'.toUpperCase()) { return "<div class='mid_ico green'><i class='fa fa-check'></i></div>"; }
    else if (STATUS.toUpperCase() === 'audit already done'.toUpperCase()) { return "<div class='mid_ico orange'><i class='fa fa-calendar-check-o'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Cancel by auditor'.toUpperCase()) { return "<div class='mid_ico red'><i class='fa fa-user-times'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Not reachable'.toUpperCase()) { return "<div class='mid_ico red'><i class='fa fa-bell'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Not reachable'.toUpperCase()) { return "<div class='mid_ico red'><i class='fa fa-bell'></i></div>"; }
    else if (STATUS.toUpperCase() === 'ok to audit'.toUpperCase()) { return "<div class='mid_ico green'><i class='fa fa-volume-control-phone'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Ringing'.toUpperCase()) { return "<div class='mid_ico orange'><i class='fa fa-check'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Ringing'.toUpperCase()) { return "<div class='mid_ico orange'><i class='fa fa-volume-control-phone'></i></div>"; }
    else if (STATUS.toUpperCase() === 'upload report'.toUpperCase()) { return "<div class='mid_ico skyblue'><i class='fa fa-upload'></i></div>"; }
    else if (STATUS.toUpperCase() === 'duplicate'.toUpperCase()) { return "<div class='mid_ico skyblue'><i class='fa fa-copy'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Not contactable'.toUpperCase()) { return "<div class='mid_ico red'><i class='fa fa-bell'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Cancel on call'.toUpperCase()) { return "<div class='mid_ico red'><i class='fa fa-phone'></i></div>"; }
    else if (STATUS.toUpperCase() === 'report generated'.toUpperCase()) { return "<div class='mid_ico green'><i class='fa fa-file'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Postpone by site'.toUpperCase()) { return "<div class='mid_ico orange'><i class='fa fa-share'></i></div>"; }
    else if (STATUS.toUpperCase() === 'qc reject'.toUpperCase()) { return "<div class='mid_ico red'><i class='fa fa-times'></i></div>"; }

    else if (STATUS.toUpperCase() === 'completed'.toUpperCase()) { return "<div class='mid_ico green'><i class='fa fa-check'></i></div>"; }
    else if (STATUS.toUpperCase() === 'verification pick'.toUpperCase()) { return "<span class='badge verify statusverificationpick cursor-pointer'>verification pick</span>"; }
    else if (STATUS.toUpperCase() === 'Cancel by site'.toUpperCase()) { return "<div class='mid_ico red'><i class='fa fa-user-times'></i></div>"; }
    else if (STATUS.toUpperCase() === 'payment reject'.toUpperCase()) { return "<span class='badge verify statuspaymentreject cursor-pointer'>payment reject</span>"; }
    else if (STATUS.toUpperCase() === 'pending approval'.toUpperCase()) { return "<span class='badge verify statuspendingapproval cursor-pointer'>pending approval</span>"; }
    else if (STATUS.toUpperCase() === 'shut down'.toUpperCase()) { return "<span class='badge verify statusshutdown cursor-pointer'>shut down</span>"; }
    else if (STATUS.toUpperCase() === 'Not Agreed'.toUpperCase()) { return "<span class='badge verify statusnotagreed cursor-pointer'>Not Agreed</span>"; }
    else if (STATUS.toUpperCase() === 'Cancel approval'.toUpperCase()) { return "<span class='badge verify statuscancelapproval cursor-pointer'>Cancel approval</span>"; }
    else if (STATUS.toUpperCase() === 'address Not found'.toUpperCase()) { return "<span class='badge verify statusaddressnotfound cursor-pointer'>address Not found</span>"; }
    else if (STATUS.toUpperCase() === 'Postpone approved'.toUpperCase()) { return "<span class='badge verify statuspostponeapproved cursor-pointer'>Postpone approved</span>"; }
    else if (STATUS.toUpperCase() === 'verification Assigned'.toUpperCase()) { return "<span class='badge verify statusverificationAssigned cursor-pointer'>verification Assigned</span>"; }
    else if (STATUS.toUpperCase() === 'qc approved'.toUpperCase()) { return "<span class='badge verify statusqcapporved cursor-pointer'>qc approved</span>"; }
    else if (STATUS.toUpperCase() === 'address Not found approved'.toUpperCase()) { return "<span class='badge verify statusaddressnotfoundapproved cursor-pointer'>address Not found approved</span>"; }
    else if (STATUS.toUpperCase() === 'Assigned'.toUpperCase()) { return "<span class='badge verify statusassigned cursor-pointer'>Assigned</span>"; }
    else if (STATUS.toUpperCase() === 'closed'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'>closed</span>"; }
    else if (STATUS.toUpperCase() === 'email'.toUpperCase()) { return "<div class='mid_ico orange'><i class='fa fa-envelope'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Meeting'.toUpperCase()) { return "<div class='mid_ico green'><i class='fa fa-briefcase'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Meeting_end'.toUpperCase()) { return "<div class='mid_ico blue'><i class='fa fa-briefcase'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Meeting_start'.toUpperCase()) { return "<div class='mid_ico green'><i class='fa fa-briefcase'></i></div>"; }

    else if (STATUS.toUpperCase() === 'got it done through fssai'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>got it done through fssai</span>"; }
    else if (STATUS.toUpperCase() === 'doesnt care'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor - pointer'>doesnt care</span>"; }
    else if (STATUS.toUpperCase() === 'doesnt believe mandate'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor - pointer'>doesnt believe mandate</span>"; }
    else if (STATUS.toUpperCase() === 'unresponsive'.toUpperCase()) { return "<span class='badge verify statuscallback cursor - pointer'>unresponsive</span>"; }
    else if (STATUS.toUpperCase() === 'Not enough business on z'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'>Not enough business on z</span>"; }
    else if (STATUS.toUpperCase() === 'Not on z anymore'.toUpperCase()) { return "<span class='badge verify statuscallback curso -pointer'>Not on z anymore</span>"; }
    else if (STATUS.toUpperCase() === 'already done'.toUpperCase()) { return "<div class='mid_ico green'><i class='fa fa-calendar-check-o'></i></div>"; }
    else if (STATUS.toUpperCase() === 'canNot afford'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>can Not afford</span>"; }
    else if (STATUS.toUpperCase() === 'revoke'.toUpperCase()) { return "<span class='badge verify statusaccepted cursor-pointer'>revoke</span>"; }
    else if (STATUS.toUpperCase() === 'opt-in'.toUpperCase()) { return "<span class='badge verify statusautoagreed cursor-pointer'>opt-in</span>"; }
    else if (STATUS.toUpperCase() === 'Converted'.toUpperCase()) { return "<div class='mid_ico green'><i class='fa fa-refresh'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Converted'.toUpperCase()) { return "<div class='mid_ico green'><i class='fa fa-refresh'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Job'.toUpperCase()) { return "<div class='mid_ico red'><i class='fa fa-user-times'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Future prospect'.toUpperCase()) { return "<div class='mid_ico green'><i class='fa fa-forward'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Meeting scheduled'.toUpperCase()) { return "<div class='mid_ico red'><i class='fa fa-clock-o'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Meeting scheduled'.toUpperCase()) { return "<div class='mid_ico green'><i class='fa fa-clock-o'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Meeting start'.toUpperCase()) { return "<div class='mid_ico green'><i class='fa fa-calendar-check-o'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Meeting start'.toUpperCase()) { return "<div class='mid_ico green'><i class='fa fa-calendar-check-o'></i></div>"; }

    else if (STATUS.toUpperCase() === 'Meeting Postpone by agent'.toUpperCase()) { return "<div class='mid_ico orange'><i class='fa fa-share'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Meeting_Postpone_by_agent'.toUpperCase()) { return "<div class='mid_ico orange'><i class='fa fa-share'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Meeting_Postpone_by_site'.toUpperCase()) { return "<div class='mid_ico orange'><i class='fa fa-times'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Meeting_Cancel_by_site'.toUpperCase()) { return "<div class='mid_ico red'><i class='fa fa-times'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Meeting Postponed by site'.toUpperCase()) { return "<div class='mid_ico orange'><i class='fa fa-share'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Meeting done'.toUpperCase()) { return "<div class='mid_ico green'><i class='fa fa-check'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Discussion done'.toUpperCase()) { return "<div class='mid_ico green'><i class='fa fa-check'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Proposal sent'.toUpperCase()) { return "<div class='mid_ico green'><i class='fa fa-check'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Follow up done'.toUpperCase()) { return "<div class='mid_ico green'><i class='fa fa-calendar-check-o'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Inbound call'.toUpperCase()) { return "<div class='mid_ico green'><i class='fa fa-phone'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Meeting end'.toUpperCase()) { return "<div class='mid_ico green'><i class='fa fa-check'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Meeting Cancel by agent'.toUpperCase()) { return "<div class='mid_ico red'><i class='fa fa-user-times'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Meeting Cancel by site'.toUpperCase()) { return "<div class='mid_ico red'><i class='fa fa-calendar-check-o'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Meeting_Cancel_by_agent'.toUpperCase()) { return "<div class='mid_ico red'><i class='fa fa-user-times'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Existing client-amc'.toUpperCase()) { return "<div class='mid_ico orange'><i class='fa fa-user'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Existing client'.toUpperCase()) { return "<div class='mid_ico orange'><i class='fa fa-user'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Intro mail sent'.toUpperCase()) { return "<div class='mid_ico green'><i class='fa fa-envelope'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Proposal revised'.toUpperCase()) { return "<div class='mid_ico orange'><i class='fa fa-file'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Proposal draft'.toUpperCase()) { return "<div class='mid_ico orange'><i class='fa fa-file'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Proposal lost'.toUpperCase()) { return "<div class='mid_ico red'><i class='fa fa-file'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Proposal Converted'.toUpperCase()) { return "<div class='mid_ico green'><i class='fa fa-file'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Proposal sent'.toUpperCase()) { return "<div class='mid_ico green'><i class='fa fa-paper-plane'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Direct call'.toUpperCase()) { return "<div class='mid_ico orange'><i class='fa fa-phone'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Dead'.toUpperCase()) { return "<div class='mid_ico red'><i class='fa fa-ban'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Not Relevant'.toUpperCase()) { return "<div class='mid_ico orange'><i class='fa fa-times'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Relevant'.toUpperCase()) { return "<span class='mid_ico green'> <i class='fa fa-check'></i></span>"; }
    else if (STATUS.toUpperCase() === 'Not contactable'.toUpperCase()) { return "<div class='mid_ico red'><i class='fa fa-bell'></i></div>"; }

    else if (STATUS.toUpperCase() === 'Language barrier  '.toUpperCase()) { return "<div class='mid_ico red'><i class='fa fa-language'></i></div>"; }
    else if (STATUS.toUpperCase() === 'Language barrier  '.toUpperCase()) { return "<div class='mid_ico red'><i class='fa fa-language'></i></div>"; }
    else { return ''; }
}


function stages(lead_stage) {


    if (lead_stage.toUpperCase() === 'New'.toUpperCase()) { return "<div class='timelinestage text-info'>New</div>"; }
    else if (lead_stage.toUpperCase() === 'in verification'.toUpperCase()) { return "<div class='timelinestage text-success'>in verification</div>"; }
    else if (lead_stage.toUpperCase() === 'Converted'.toUpperCase()) { return "<div class='timelinestage text-info text-success'>Converted</div>"; }
    else if (lead_stage.toUpperCase() === 'Relevant'.toUpperCase()) { return "<div class='timelinestage text-success'>Relevant</div>"; }
    else if (lead_stage.toUpperCase() === 'Not Relevant'.toUpperCase()) { return "<div class='timelinestage text-danger'>Not Relevant</div>"; }
    else if (lead_stage.toUpperCase() === 'Interested'.toUpperCase()) { return "<div class='timelinestage text-success'>Interested</div>"; }
    else if (lead_stage.toUpperCase() === 'Not Interested'.toUpperCase()) { return "<div class='timelinestage text-danger'>Not Interested</div>"; }
    else if (lead_stage.toUpperCase() === 'Proposal sent'.toUpperCase()) { return "<div class='timelinestage text-success'>Proposal sent</div>"; }
    else { return ''; }

}



function renderdatastatus(str) {
    res = str.replace("<span class='badge verify statusnew cursor-pointer'>", "");
    res1 = res.replace("<span class='badge verify statusautoagreed cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge statusauditnotcreated verify cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statusqcassined cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statusstarted cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statusqcdone cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statuspostponebyauditor cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statusaccepted cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statusagreedpostpone cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statuspostponeoncall cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statuscallback cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statusagreed cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statuswrongnumber cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statuspaymentapproved cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statusauditalreadydone cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statuscancelbyauditor cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statusnotreachable cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statusoktoaudit cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statusringing cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statusuploadreport cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statusduplicate cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statuslanguagebarrier cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statusnotcontactable cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statuscanceloncall cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statusreportgenerated cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statuspostponebysite cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statusqcreject cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statuscompleted cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statusverificationpick cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statuscancelbysite cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statuspaymentreject cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statuspendingapproval cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statusshutdown cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statusnotagreed cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statuscancelapproval cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statusaddressnotfound cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statuspostponeapproved cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statusverificationassigned cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statusqcapporved cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statusaddressnotfoundapproved cursor-pointer'>", "");
    res1 = res1.replace("<span class='badge verify statusassigned cursor-pointer'>", "");
    res2 = res1.replace("</span>", "");
    return res2;
}
function stagesbadge(LEAD_STAGE1) {
    if (LEAD_STAGE1 === 'NEW'.toUpperCase()) { return "<span class='badge verify statusnew cursor-pointer''>New</span>"; }
    else if (LEAD_STAGE1 === 'IN VERIFICATION'.toUpperCase()) { return "<span class='badge verify statusaccepted  cursor-pointer'>IN VERIFICATION</span>"; }
    else if (LEAD_STAGE1 === 'CONVERTED'.toUpperCase()) { return "<span class='badge verify statusqcdone cursor-pointer'>CONVERTED</span>"; }
    else if (LEAD_STAGE1 === 'RELEVANT'.toUpperCase()) { return "<span class='badge verify statusqcdone cursor-pointer'>RELEVANT</div>"; }
    else if (LEAD_STAGE1 === 'NOT RELEVANT'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'>NOT RELEVANT</span>"; }
    else if (LEAD_STAGE1 === 'INTERESTED'.toUpperCase()) { return "<span class='badge verify statusqcdone cursor-pointer'>INTERESTED</span>"; }
    else if (LEAD_STAGE1 === 'Not interested'.toUpperCase()) { return "<span class='badge verify statuscallback cursor-pointer'>Not interested</span>"; }
    else if (LEAD_STAGE1 === 'PROPOSAL SENT'.toUpperCase()) { return "<span class='badge verify statusqcdone cursor-pointer'>PROPOSAL SENT</span>"; }
    else { return ''; }

}
function redirectsalespage(status) {
    document.location.href = 'Sales_Lead_details.aspx?status=' + status, true;
}