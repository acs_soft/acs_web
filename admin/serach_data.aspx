﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/ACS.Master" AutoEventWireup="true" CodeBehind="serach_data.aspx.cs" Inherits="ACS.admin.serach_data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .badge {
            border-radius: 5px !important;
            font-size: 17px;
            line-height: 1;
            padding: 0.375rem 0.5625rem;
            font-weight: 700 !important;
            color: #ffffff !important;
            font-size: 17px !important;
            border: 1px solid #52CDFF;
        }

        .table > :not(:last-child) > :last-child > *, .jsgrid .jsgrid-table > :not(:last-child) > :last-child > * {
            border-bottom-color: #28a745;
            background-color: #28a745;
            color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper">
        <div class="row p-2">
            <div class="col-md-3">
            </div>
            <div class="col-md-4 p-2">

                <input type="text" class="form-control input-lg p-2" name="fname" id="sea" placeholder="Enter Keyword" style="width: 100%;">
            </div>
            <div class="col-md-2 p-2">


                <input type="button" class="btn btn-primary" value="UNIVERSAL SEARCH" onclick="showDetails()" style="padding: 10px 10px;">
            </div>
            <div class="col-md-12">
                <div class="loader mt-5 mb-5" style="margin: 0px auto; display: none;"></div>
            </div>
        </div>
        <div class="row  p-2  mb-2 card-body" id="s1" style="display: none; background-color: white; border-radius: 5px;">

            <div class="col-md12 mb-2">
                <h4>Google Search Results</h4>
            </div>
            <div class="col-md-4 mb-2"><i class="fa fa-google fa-2x"></i><a class="btn mb-2" id="btn1" target="_blank" href="#"></a><span class="badge badge-info" id="g1resultc1">0</span></div>
            <div class="col-md-4 mb-2"><i class="fa fa-google fa-2x"></i><a class="btn mb-2" id="btn2" target="_blank" href="#"></a><span class="badge badge-info" id="g1resultc2">0</span></div>
            <div class="col-md-4 mb-2"><i class="fa fa-google fa-2x"></i><a class="btn mb-2" id="btn3" target="_blank" href="#"></a><span class="badge badge-info" id="g1resultc3">0</span></div>
            <div class="col-md-4 mb-2"><i class="fa fa-google fa-2x"></i><a class="btn mb-2" id="btn4" target="_blank" href="#"></a><span class="badge badge-info" id="g1resultc4">0</span></div>
            <div class="col-md-4 mb-2"><i class="fa fa-google fa-2x"></i><a class="btn mb-2" id="btn5" target="_blank" href="#"></a><span class="badge badge-info" id="g1resultc5">0</span></div>
            <div class="col-md-4 mb-2"><i class="fa fa-google fa-2x"></i><a class="btn mb-2" id="btn6" target="_blank" href="#"></a><span class="badge badge-info" id="g1resultc6">0</span></div>
        </div>
        <div class="row  p-2  mb-2 card-body" id="s2" style="display: none; background-color: white; border-radius: 5px;">
            <div class="col-md12 mb-2">
                <h4>Social Media Search Results</h4>
            </div>
            <div class="col-md-4 mb-2"><i class="fa fa-facebook-official fa-2x"></i><a class="btn mb-2" id="btnface" target="_blank" href="#"></a></div>
            <div class="col-md-4 mb-2"><i class="fa fa-linkedin fa-2x"></i><a class="btn mb-2" id="btninin" target="_blank" href="#"></a></div>
            <div class="col-md-4 mb-2"><i class="fa fa-twitter fa-2x"></i><a class="btn mb-2" id="btntwitter" target="_blank" href="#"></a></div>
            <div class="col-md-4 mb-2"><i class="fa fa-instagram fa-2x"></i><a class="btn mb-2" id="btninsta" target="_blank" href="#"></a></div>


        </div>

        <div class="row  p-2  mb-2 card-body" style="border-radius: 5px;">


            <div class="col-md-12 p-3" id="s5" style="display: none; background-color: white;">
                <div class="col-md-12 mb-2">
                    <h4>Disqualified Director List <span class="badge badge-info" id="decount">0</span></h4>
                </div>

                <div class="col-md-12">
                    <table id="datatabel1" class="table table-striped table-bordered dt-responsive nowrap" style="width: 100%">
                        <thead>
                            <tr class="text-center">

                                <th class="datatabel-head">DIN</th>
                                <th class="datatabel-head">Director Name</th>
                                <th class="datatabel-head">Company Name</th>
                                <th class="datatabel-head">CIN</th>
                                <th class="datatabel-head">ROC Location</th>
                                <th class="datatabel-head">Company Status			</th>
                                <th class="datatabel-head">Status</th>
                                <th class="datatabel-head">Period from</th>
                                <th class="datatabel-head">Period till</th>


                            </tr>
                        </thead>
                    </table>


                </div>
            </div>

            <div class="col-md-12 p-3" id="s6" style="display: none; background-color: white;">
                <div class="col-md-12 mb-2">
                    <h4>Politically Exposed List <span class="badge badge-info" id="poecount">0</span></h4>
                </div>

                <div class="col-md-12">
                    <table id="datatabel2" class="table table-striped table-bordered dt-responsive nowrap" style="width: 100%">
                        <thead>
                            <tr class="text-center">


                                <th class="datatabel-head">Candidate_name</th>
                                <th class="datatabel-head">Constituency</th>
                                <th class="datatabel-head">Party</th>
                                <th class="datatabel-head">Criminal_case</th>
                                <th class="datatabel-head">Education</th>
                                <th class="datatabel-head">Total_assets</th>
                                <th class="datatabel-head">Liabilities</th>


                            </tr>
                        </thead>
                    </table>


                </div>
            </div>
            <div class="col-md-6 p-3" id="s3" style="display: none; background-color: white;">
                <div class="col-md12 mb-2">
                    <h4>My Neta</h4>
                </div>
                <a class="badge badge-dark mb-1" id="wikiopen1" target="_blank" href="#" title="Open In new tab" style="position: absolute; top: 16px; right: 3%; z-index: 999;"><i class="fa fa-external-link fa-1x  p-1"></i><a class=""></a></a>
                <div class="embed-responsive embed-responsive-16by9" style="box-shadow: 0px 0px 10px grey;">
                    <iframe id="mynetaFrame" name="innerFrame" class="embed-responsive-item" frameborder="0" sandbox="allow-scripts allow-popups allow-forms allow-same-origin allow-popups-to-escape-sandbox allow-downloads" frameborder="0" allowfullscreen="" src="https://www.myneta.info/"></iframe>
                </div>


            </div>
            <div class="col-md-6 p-3" id="s4" style="display: none; background-color: white;">
                <div class="col-md12 mb-2">
                    <h4>Wikipedia</h4>
                </div>
                <a class="badge badge-dark mb-1" id="wikiopen" target="_blank" href="#" title="Open In new tab" style="position: absolute; top: 16px; right: 3%; z-index: 999;"><i class="fa fa-external-link fa-1x  p-1"></i><a class=""></a></a>
                <div class="embed-responsive embed-responsive-16by9" style="box-shadow: 0px 0px 10px grey;">
                    <iframe id="wikiFrame" name="innerFrame" class="embed-responsive-item" sandbox="allow-scripts allow-popups allow-forms allow-same-origin allow-popups-to-escape-sandbox allow-downloads" frameborder="0" allowfullscreen="" src="https://en.wikipedia.org/wiki" style="overflow: auto;"></iframe>
                </div>
            </div>
        </div>

        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->

        <!-- partial -->
    </div>
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="js/cookie.js"></script>
    <script type="text/javascript">


      


        function get_director_list(datajson) {
            $.getJSON(datajson, { format: "json" }).done(function (data) {
                // Object split 
                myjson = data.data;
                errorflag = data.CODE;

                if (errorflag == 500) {
                    document.location.href = '500Error.aspx', true;
                }
                else {
                    var datatabel1 = $('#datatabel1').DataTable({


                        "dom": 'frtip',
                        "destroy": true,
                        buttons: {
                            dom: {
                                button: {
                                    className: 'btn btn-primary p-1'
                                }
                            },
                            buttons: [
                                { extend: 'excelHtml5', text: 'Download Excel', title: 'director_list' }

                            ]
                        },

                        "order": [],
                        "pageLength": 10,
                        "lengthMenu": [[10, 25, 50, 100, 200, 500, 1000, -1], [10, 25, 50, 100, 200, 500, 1000, "All"]],
                        initComplete: function () {


                            this.api().columns([1]).every(function () {
                                var column = this;
                                var title = this.header();

                                var select = $('<select class="form-control col-md-12 col-sm-12 col-xs-12 filterselect" id="' + $(title).html() + '"><option selected="true" disabled="disabled">SELECT ' + $(title).html() + '</option><option value="">Show All</option></select>')
                                    .appendTo($('#tfoot'))
                                    //.appendTo($(column.header()))
                                    .on('change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                        );
                                        column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                        datatabel1.draw();

                                    });

                                column.cells('', column[0]).render('display').unique().sort().each(function (d, j) {
                                    if (d === '') {
                                        d = '(Empty)'
                                    }
                                    select.append('<option value="' + d + '">' + d + '</option>')
                                });
                            });
                        },
                        "pagingType": "full",
                        "infoFiltered": true,

                        //-----------------------Ajax data code---------------------------------------->
                        aaData: myjson,

                        "columns": [

                            { "data": "DIN" },
                            { "data": "DIRECTOR_NAME" },
                            { "data": "COMPANY_NAME" },
                            { "data": "CIN" },
                            { "data": "ROC_LOCATION" },
                            { "data": "COMPANY_STATUS" },
                            { "data": "STATUS" },
                            { "data": "PERIOD_FROM" },
                            { "data": "PERIOD_TILL" }
                        ],
                        "columnDefs": [
                            {
                                orderable: false,
                                targets: [0, 2]
                            },

                            {
                                targets: [1],
                                render: function (data, type, row, meta) {
                                    return type === 'filter' ?
                                        data === '' ? '(Empty)' : data : data;
                                }
                            },
                        ]
                        , order: [1, 'asc']

                    });

                    //Data tabel date ase/desc code 
                    $.fn.dataTableExt.oSort['time-date-sort-pre'] = function (value) {
                        return Date.parse(value);
                    };
                    $.fn.dataTableExt.oSort['time-date-sort-asc'] = function (a, b) {
                        return a - b;
                    };
                    $.fn.dataTableExt.oSort['time-date-sort-desc'] = function (a, b) {
                        return b - a;
                    };




                }
                $('#decount').html(datatabel1.data().count());


            });

        }
        function get_poly_list(datajson) {
            $.getJSON(datajson, { format: "json" }).done(function (data) {
                // Object split 
                myjson = data.data;
                errorflag = data.CODE;

                if (errorflag == 500) {
                    document.location.href = '500Error.aspx', true;
                }
                else {
                    var datatabel2 = $('#datatabel2').DataTable({


                        "dom": 'frtip',
                        "destroy": true,
                        buttons: {
                            dom: {
                                button: {
                                    className: 'btn btn-primary p-1'
                                }
                            },
                            buttons: [
                                { extend: 'excelHtml5', text: 'Download Excel', title: 'director_list' }

                            ]
                        },

                        "order": [],
                        "pageLength": 10,
                        "lengthMenu": [[10, 25, 50, 100, 200, 500, 1000, -1], [10, 25, 50, 100, 200, 500, 1000, "All"]],
                        initComplete: function () {


                            this.api().columns([1]).every(function () {
                                var column = this;
                                var title = this.header();

                                var select = $('<select class="form-control col-md-12 col-sm-12 col-xs-12 filterselect" id="' + $(title).html() + '"><option selected="true" disabled="disabled">SELECT ' + $(title).html() + '</option><option value="">Show All</option></select>')
                                    .appendTo($('#tfoot'))
                                    //.appendTo($(column.header()))
                                    .on('change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                        );
                                        column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                        datatabel2.draw();

                                    });

                                column.cells('', column[0]).render('display').unique().sort().each(function (d, j) {
                                    if (d === '') {
                                        d = '(Empty)'
                                    }
                                    select.append('<option value="' + d + '">' + d + '</option>')
                                });
                            });
                        },
                        "pagingType": "full",
                        "infoFiltered": true,

                        //-----------------------Ajax data code---------------------------------------->
                        aaData: myjson,

                        "columns": [


                            { "data": "CANDIDATE_NAME" },
                            { "data": "CONSTITUENCY" },
                            { "data": "PARTY" },
                            { "data": "CRIMINAL_CASE" },
                            { "data": "EDUCATION" },
                            { "data": "TOTAL_ASSETS" },
                            { "data": "LIABILITIES" }
                        ],
                        "columnDefs": [
                            {
                                orderable: false,
                                targets: [0, 2]
                            },

                            {
                                targets: [1],
                                render: function (data, type, row, meta) {
                                    return type === 'filter' ?
                                        data === '' ? '(Empty)' : data : data;
                                }
                            },
                        ]
                        , order: [0, 'asc']

                    });

                    //Data tabel date ase/desc code 
                    $.fn.dataTableExt.oSort['time-date-sort-pre'] = function (value) {
                        return Date.parse(value);
                    };
                    $.fn.dataTableExt.oSort['time-date-sort-asc'] = function (a, b) {
                        return a - b;
                    };
                    $.fn.dataTableExt.oSort['time-date-sort-desc'] = function (a, b) {
                        return b - a;
                    };




                }

                $('#poecount').html(datatabel2.data().count());

            });
        }
        function showgoogleDetails() {
            $('#s1').show();
            $('#s2').hide();
            $('#s3').hide();
            $('#s4').hide();
            $('#s5').hide();
            $('#s6').hide();
        }
        function showsocialDetails() {
            $('#s1').hide();
            $('#s2').show();
            $('#s3').hide();
            $('#s4').hide();
            $('#s5').hide();
            $('#s6').hide();

        }
        function showmynetaDetails() {
            $('#s1').hide();
            $('#s2').hide();
            $('#s3').show();
            $('#s4').hide();
            $('#s5').hide();
            $('#s6').hide();

        }
        function showwikiDetails() {
            $('#s1').hide();
            $('#s2').hide();
            $('#s3').hide();
            $('#s4').show();
            $('#s5').hide();
            $('#s6').hide();

        }
        function showexistingDetails() {
            $('#s1').hide();
            $('#s2').hide();
            $('#s3').hide();
            $('#s4').hide();
            $('#s5').show();
            $('#s6').hide();

        }
        function showpolyexpDetails() {
            $('#s1').hide();
            $('#s2').hide();
            $('#s3').hide();
            $('#s4').hide();
            $('#s5').hide();
            $('#s6').show();

        }

        function getgooglecount(searchkeyword, divid) {
            var countno = '';

            $.ajax({
                type: 'get',

                url: "https://google-search3.p.rapidapi.com/api/v1/search/q=" + searchkeyword,

                headers: {
                    "x-user-agent": "desktop",
                    "x-proxy-location": "EU",
                    "x-rapidapi-host": "google-search3.p.rapidapi.com",
                    "x-rapidapi-key": "b5f539c9c8mshcf6f2f442d3c60ep1f5ea4jsn585131f9df16"
                },
                dataType: "json",
                contentType: 'application/json;charset=utf-8 ',
                success: function (data) {
                    countno = data.total;

                    $('#' + divid).html(countno);
                }


            });


        }
        function showDetails() {
            $('#s1').hide();
            $('#s2').hide();
            $('#s3').hide();
            $('#s4').hide();
            $('#s5').hide();
            $('#s6').hide();

            var a = document.getElementById('sea').value;
            if (a == '' || a == ' ') {
                info_msg("Please enter keyword !!");
            }
            else {
                $('.loader').show();
                setTimeout(function () {
                    $('.loader').hide();
                    $('#s1').show();
                    $('#s2').show();
                    $('#s3').hide();
                    $('#s4').hide();
                    $('#s5').show();
                    $('#s6').show();

                    datajson_directlist = "http://dvs.allcheckservices.com/api/Service1.svc/disqualified_director_universal_search/" + a;
                    get_director_list(datajson_directlist);

                    datajson_directlist1 = "http://dvs.allcheckservices.com/api/Service1.svc/politically_exposed_universal_search/" + a;
                    get_poly_list(datajson_directlist1);

                    var a1 = '"' + 'Any fraud ' + a + '"';
                    var a2 = '"' + 'Any bank notice against ' + a + '"';
                    var a3 = '"' + 'Any court case against ' + a + '"';
                    var a4 = '"' + 'Any criminal case ' + a + '"';
                    var a5 = '"' + 'arrtsed ' + a + '"';
                    var a6 = '"' + 'bank auction notice ' + a + '"';
                    var awiki = a;
                   // getgooglecount(a1, 'g1resultc1');

                    //alert(g1resultc1);
                    ////var g1resultc2 = getgooglecount(a2);
                    ////var g1resultc3 = getgooglecount(a3);
                    ////var g1resultc4 = getgooglecount(a4);
                    ////var g1resultc5 = getgooglecount(a5);
                    ////var g1resultc6 = getgooglecount(a6);
                    //$('#g1resultc1').html(g1resultc1);
                    ////$('#g1resultc2').html(g1resultc2);
                    ////$('#g1resultc3').html(g1resultc3);
                    ////$('#g1resultc4').html(g1resultc4);
                    ////$('#g1resultc5').html(g1resultc5);
                    ////$('#g1resultc6').html(g1resultc6);


                    var wikiurl = 'https://en.wikipedia.org/wiki/' + awiki;
                    var mynetaFrame = 'https://www.myneta.info/search_myneta.php?q=' + awiki;
                    var btn1 = 'https://www.google.com/search?q=' + a1;
                    var btn2 = 'https://www.google.com/search?q=' + a2;
                    var btn3 = 'https://www.google.com/search?q=' + a3;
                    var btn4 = 'https://www.google.com/search?q=' + a4;
                    var btn5 = 'https://www.google.com/search?q=' + a5;
                    var btn6 = 'https://www.google.com/search?q=' + a6;

                    // var btnface = 'https://www.facebook.com/search/top?q=' + a;
                    //var btninin = 'https://www.linkedin.com/search/results/people/?keywords=' + a;

                    var btnface = 'https://www.facebook.com/public/' + a;
                    var btninin = 'https://www.linkedin.com/search/results/all/?keywords=' + a;
                    var btntwitter = 'https://www.google.com/search?q=Twitter ' + a;
                    var btninsta = 'https://www.google.com/search?q=Instagram ' + a;




                    $('#wikiFrame').attr('src', wikiurl);
                    $('#wikiopen').attr('href', wikiurl);
                    $('#wikiopen1').attr('href', mynetaFrame);
                    $('#mynetaFrame').attr('src', mynetaFrame);

                    $('#btn1').attr('href', btn1);
                    $('#btn2').attr('href', btn2);
                    $('#btn3').attr('href', btn3);
                    $('#btn4').attr('href', btn4);
                    $('#btn5').attr('href', btn5);
                    $('#btn6').attr('href', btn6);
                    $('#btnface').attr('href', btnface);
                    $('#btninin').attr('href', btninin);
                    $('#btntwitter').attr('href', btntwitter);
                    $('#btninsta').attr('href', btninsta);

                    $("#btn1").text(a1);
                    $("#btn2").text(a2);
                    $("#btn3").text(a3);
                    $("#btn4").text(a4);
                    $("#btn5").text(a5);
                    $("#btn6").text(a6);

                    $("#btnface").text("Facebook " + a);
                    $("#btninin").text("Linkedin " + a);
                    $("#btntwitter").text("Twitter " + a);
                    $("#btninsta").text("Instagram " + a);
                    datatabel1.clear();
                    datatabel1.destroy();
                    datatabel2.clear();
                    datatabel2.destroy();
                }, 2000);



            }

        }


    </script>
</asp:Content>


