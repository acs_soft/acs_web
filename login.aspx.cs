﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;
using System.Web;


namespace ACS
{
    public partial class login : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {


            //if (Request.Cookies["QUNTX0NPT0tJRVNfTkFNRQ"] != null && Request.Cookies["QUNTX0NPT0tJRVNfTkFNRQ"]["url"] != "")
            //{
            //    string URL = "admin/" + Request.Cookies["QUNTX0NPT0tJRVNfTkFNRQ"]["url"].ToString();

            //    Response.Redirect(URL);
            //}
            //else
            //{
            //   // Response.Redirect("admin/logout.aspx");
            //}

        }

        [System.Web.Services.WebMethod(EnableSession = true)]
        public static string User_login(string username, string password)
        {

            if (username != null)
            {
                if (password != null)
                {
                    try
                    {
                        SqlConnection sqlCnn;
                        string CS = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                        string CS_P = ConfigurationManager.ConnectionStrings["ACS_Center"].ConnectionString;

                        sqlCnn = new SqlConnection(CS);
                        DataTable dt = new DataTable();
                        using (SqlConnection con = new SqlConnection(CS_P))
                        {

                            using (SqlCommand cmd = new SqlCommand("USP_USER_LOGIN"))
                            {

                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Connection = con;
                                cmd.Parameters.AddWithValue("@type", "USER_LOGIN_WEB");
                                cmd.Parameters.AddWithValue("@SOLITARY_MOBILE", username);
                                cmd.Parameters.AddWithValue("@SOLITARY_PASS", password);

                                con.Open();
                                using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                                {
                                    sda.Fill(dt);
                                }

                                string dbresult = dt.Rows[0][0].ToString();
                                if (dbresult == "0")
                                {

                                    return string.Format("0");
                                }
                                else
                                {
                                    string dbmobile = dt.Rows[0]["SOLITARY_MOBILE"].ToString();
                                    if (dbmobile.ToString() == username)
                                    {
                                        string code = dt.Rows[0]["SOLITARY_ID"].ToString().ToUpper();
                                        string role = dt.Rows[0]["ROLE_ID"].ToString().ToUpper();
                                        string token = dt.Rows[0]["SOLITARY_FIREBASE_TOKEN"].ToString();
                                        string name = dt.Rows[0]["F_NAME"].ToString().Trim();
                                        string l_name = dt.Rows[0]["L_NAME"].ToString().Trim();
                                        string mobile = dt.Rows[0]["SOLITARY_MOBILE"].ToString();
                                        string emailid = dt.Rows[0]["EMAIL_ID"].ToString();
                                        string designation = dt.Rows[0]["ROLE_ID"].ToString();
                                        string dashboardpage = dt.Rows[0]["SOLITARY_TOKEN"].ToString();
                                        string redirecturl = dt.Rows[0]["DASHBOARD_URL"].ToString();
                                        login login = new login();
                                        
                                        string redirecturl1 = "serach_data.aspx";
                                        HttpCookie cookie = new HttpCookie("QUNTX0NPT0tJRVNfTkFNRQ");
                                        cookie.Values["solitary_code"] = code;
                                        cookie.Values["solitary_role"] = role;
                                        cookie.Values["solitary_token"] = token;
                                        cookie.Values["name"] = name;
                                        cookie.Values["last_name"] = l_name;
                                        cookie.Values["mobile"] = mobile;
                                        cookie.Values["emailid"] = emailid;
                                        cookie.Values["designation"] = designation;
                                        cookie.Values["dashboardpage"] = dashboardpage;
                                        cookie.Values["url"] = redirecturl;
                                        cookie.Values["DEPT_ID"] = string.IsNullOrEmpty(dt.Rows[0]["DEPT_ID"].ToString()) ? null : dt.Rows[0]["DEPT_ID"].ToString();
                                      

                                        cookie.Expires = DateTime.Now.AddDays(1);
                                        HttpContext.Current.Response.Cookies.Add(cookie);

                                        return string.Format(redirecturl1);
                                    }
                                    else
                                    {
                                        return string.Format("1");
                                    }


                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.Write(ex);

                    }

                    return string.Format("1");
                }
                else
                {

                    return string.Format("2");

                }
            }
            else
            {
                return string.Format("3");

            }
        }


       
    }
}