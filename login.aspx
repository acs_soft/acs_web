﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="ACS.login" %>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/owl.carousel.min.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- Style -->
    <link rel="stylesheet" href="css/style.css">

    <title>Login Page</title>
</head>
<body>



    <div class="content">
        <div class="container">
            <div class="row">
                <div id="commonerrorwrap" class="text-center pt-2 col-md-12 col-sm-6 col-xs-12 mastererror" style="background-color: rgb(244, 67, 54); display: none;"><i class="fa fa-window-close"></i>&nbsp;&nbsp;<label id="lblerrorpage">Please Select Category.</label></div>
                <div class="col-md-6">
                    <img src="images/undraw_remotely_2j6y.svg" alt="Image" class="img-fluid">
                </div>
                <div class="col-md-6 contents">
                    <div class="row justify-content-center">
                        <div class="col-md-8">
                            <div class="mb-0" style="text-align: center;">
                                <img src="images/logo.jpg" alt="Image" class="img-fluid">
                            </div>
                            <form action="#" method="post">
                                <div class="form-group first mb-4">
                                    <label for="username">Username</label>
                                    <input type="text" class="form-control" id="txtusername" maxlength="10">
                                </div>
                                <div class="form-group last mb-4">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" id="txtpassword" maxlength="30">
                                </div>

                                <div class="d-flex mb-5 align-items-center">
                                    <label class="control control--checkbox mb-0">
                                        <span class="caption">Remember me</span>
                                        <input type="checkbox" checked="checked" />
                                        <div class="control__indicator"></div>
                                    </label>
                                    <!-- <span class="ml-auto"><a href="#" class="forgot-pass">Forgot Password</a></span>  -->
                                </div>

                                <input type="button" value="Log In" id="loginbutton" class="btn btn-block btn-primary">
                                <label id="lblerror1" style="color: #F44336; background-color: white; text-align: center; width: 100%; margin-top: 10px;" />


                            </form>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>


    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <script src="js/main.js"></script>
    <script src="admin/js/login.js"></script>
    <script src="admin/js/page.js"></script>

    <script src="admin/js/cookie.js"></script>

    <script>

        //$("#loginbutton").click(function () {
        //    var uname = $("#txtusername").val();
        //    var password = $("#txtpassword").val();


        //    if (uname != '' && password != '') {
        //        if (uname == 'admin') {
        //            if (password == 'admin') {
        //                window.location.href = "admin/serach_data.aspx";
        //            }
        //            else {
        //                $("#lblerror1").text("Wrong password");
        //            }
        //        }
        //        else {
        //            $("#lblerror1").text("Wrong username");
        //        }
        //    }
        //    else {
        //        $("#lblerror1").text("Please enter username and password");
        //    }

        //});

        var ck = new Cookiefunction();
        allcharacterallow(["txtusername"]);
        var varnumber = document.getElementById("txtusername");
        varnumber.addEventListener("keyup", function (event) {
            if (event.keyCode === 13) {
                event.preventDefault();
                document.getElementById("loginbutton").click();
            }
        });
        var varpassword = document.getElementById("txtpassword");
        varpassword.addEventListener("keyup", function (event) {
            if (event.keyCode === 13) {
                event.preventDefault();
                document.getElementById("loginbutton").click();
            }
        });
        $(document).ready(function () {

            // alert(ck.url);
            //if (ck.url != null) {

            //    document.location.href = 'admin/' + ck.url, true;
            //}
            //a();

        });
    </script>
</body>
</html>

